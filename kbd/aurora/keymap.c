#include QMK_KEYBOARD_H


/// OVERRIDES
const key_override_t comma_ovr = ko_make_basic(MOD_MASK_SHIFT, KC_COMM, KC_SCLN);
const key_override_t dot_ovr = ko_make_basic(MOD_MASK_SHIFT, KC_DOT, KC_COLN);

// This globally defines all key overrides to be used
const key_override_t **key_overrides = (const key_override_t *[]){
    &comma_ovr,
    &dot_ovr,
    NULL // Null terminate the array of overrides!
};


enum layers {
    MTGAP = 0,
    SYMBOLS = 1,
    MORE = 2,
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [MTGAP] = LAYOUT(
        KC_Y, KC_P, KC_O, KC_U, KC_J,  KC_K, KC_D, KC_L, KC_C, KC_W,
        KC_I, KC_N, KC_E, KC_A, KC_COMM,  KC_M, KC_H, KC_T, KC_S, KC_R,
        OSM(MOD_LCTL), KC_ESC, KC_ENT, KC_DOT, KC_SLSH,  KC_B, KC_F, KC_G, KC_V, KC_X,
        KC_BSPC, OSM(MOD_LSFT),  OSL(SYMBOLS), KC_SPC
    ),

    [SYMBOLS] = LAYOUT(
        KC_7, KC_5, KC_3, KC_1, KC_9,  KC_8, KC_0, KC_2, KC_4, KC_6,
        KC_QUOT, KC_MINS, KC_LPRN, KC_LCBR, KC_Z,  KC_Q, KC_RCBR, KC_RPRN, KC_EQL, KC_DQT,
        OSM(MOD_LGUI), _______, KC_LT, KC_LBRC, KC_TAB,  KC_PLUS, KC_RBRC, KC_GT, KC_ASTR, OSM(MOD_LALT),
        _______, OSL(MORE),  XXXXXXX, KC_DEL
    ),

    [MORE] = LAYOUT(
        QK_RBT, XXXXXXX, XXXXXXX, KC_PGUP, KC_DOWN,  KC_UP, KC_PGDN, KC_RGHT, XXXXXXX, XXXXXXX,
        KC_GRV, KC_TILD, KC_END, KC_HOME, S(KC_Z),  S(KC_Q), KC_LEFT, KC_EXLM, KC_DLR, KC_HASH,
        XXXXXXX, _______, XXXXXXX, KC_PERC, KC_BSLS,  KC_AT, KC_CIRC, KC_PIPE, XXXXXXX, XXXXXXX,
        _______, XXXXXXX,  TO(MTGAP), KC_SPC
    ),
};

void oneshot_mods_changed_user(uint8_t mods) {
    // switch to MTGAP layer when one shot GUI or ALT
    if (mods & MOD_MASK_ALT) {
        layer_clear();
    }
    if (mods & MOD_MASK_GUI) {
        layer_clear();
    }
}


#ifdef RGBLIGHT_ENABLE
void keyboard_post_init_user(void) {
    rgblight_enable_noeeprom(); // enables RGB, without saving settings
    rgblight_sethsv_noeeprom(HSV_RED); // sets the color to red without saving
    rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_SWIRL);
    /* rgblight_mode_noeeprom(RGBLIGHT_MODE_CHRISTMAS); */
    /* rgblight_mode_noeeprom(RGBLIGHT_MODE_RAINBOW_MOOD); */
}
#endif

// the default aurora oled stuff is good enough for me
/*
  #ifdef OLED_ENABLE
  bool oled_task_user(void) {
  // A 128x32 OLED rotated 90 degrees is 5 characters wide and 16 characters tall
  // This example string should fill that neatly
  const char *text = PSTR("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789!@#$%^&*()[]{}-=_+?");

  if (is_keyboard_master()) {
  oled_write_P(text, false);
  } else {
  oled_write_P(text, false);
  }
  return false;
  }
  #endif
*/
