# NOTE: These are already enabled by default at the revision level
#ENCODER_ENABLE = yes
#OLED_ENABLE = yes

# RGB Matrix is enabled at the revision level,
# while we use the regular RGB underglow for testing
RGB_MATRIX_ENABLE = no
RGBLIGHT_ENABLE = yes
OLED_ENABLE = yes
COMBO_ENABLE = no
KEY_OVERRIDE_ENABLE = yes
# AUTO_SHIFT_ENABLE = yes
