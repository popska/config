;;; -*- lexical-binding: t; buffer-read-only: t; -*-

;; The default is 800 kilobytes.  Measured in bytes.
;; this *might* speed up initialization
(setq gc-cons-threshold (* 50 1000 1000))

(eval-when-compile
  (defconst m-eq? (string= (system-name) "equanimity"))
  (defconst m-su? (string= (system-name) "sublimity"))
  (defconst m-ww? (eq system-type 'windows-nt))
  (defconst m-wl? (not (or m-eq? m-su? m-ww?))))

;; on windows load the packages copied over from guix
(when m-ww?
  (seq-do (lambda (dir)
            (unless (string-match-p "\\.$" dir) ; don't match . and ..
              (add-to-list 'load-path dir)))
          (directory-files (expand-file-name "guix-packages/" user-emacs-directory) t)))

(setopt native-comp-async-report-warnings-errors nil)

(require 'use-package)

(use-package diminish
  :if t
  :defer t)

(defconst m-all-states '(normal insert visual replace operator motion emacs))

;; TODO: checkout =defvar-keymap= added in 29!

(use-package general
  :if t
  :demand t
  :config
  ;;; define the overall prefix map
  (defvar m-prefix-map (make-sparse-keymap))
  (general-create-definer m-leader
    :keymaps m-all-states
    :prefix "SPC"
    :global-prefix "C-c"
    :prefix-map 'm-prefix-map)
  ;; define it in override map to really make sure it happens everywhere
  (general-define-key
   :states '(normal visual motion)
   :keymaps 'override
   "SPC" m-prefix-map)
  (general-define-key
   :states m-all-states
   :keymaps 'override
   "C-c" m-prefix-map)
  (general-define-key
   :keymaps 'global-map
   "C-c" m-prefix-map)

  ;;; define infix maps
  ;; keymap for a bunch of toggle bindings
  (defvar m-toggle-map (make-sparse-keymap))
  (general-create-definer m-toggle-def
    :keymaps m-all-states
    :prefix "SPC t"
    :global-prefix "C-c t"
    :prefix-map 'm-toggle-map
    ;; make which key useful
    "" (cons "toggles" m-toggle-map))

  ;; keymap for magit
  (defvar m-git-map (make-sparse-keymap))
  (general-create-definer m-git-def
    :keymaps m-all-states
    :prefix "SPC g"
    :global-prefix "C-c g"
    :prefix-map 'm-git-map
    ;; make which key useful
    "" (cons "git" m-git-map))

  ;; make SPC h same as C-h
  (m-leader 'normal
    "h" (cons "help" help-map))
  (defconst m-help-map help-map)

  ;; buffer commands
  (defvar m-buffer-map (make-sparse-keymap))
  (general-create-definer m-buffer-def
    :keymaps m-all-states
    :prefix "SPC b"
    :global-prefix "C-c b"
    :prefix-map 'm-buffer-map
    "" (cons "buffer/file" m-buffer-map))

  ;; project commands (SPC p same as C-x p)
  (m-leader 'normal
    "p" (cons "project" project-prefix-map))
  (defconst m-project-map project-prefix-map)

  ;; code commands
  (defvar m-code-map (make-sparse-keymap))
  (general-create-definer m-code-def
    :keymaps m-all-states
    :prefix "SPC c"
    :global-prefix "C-c c"
    :prefix-map 'm-code-map
    "" (cons "code" m-code-map))

  ;; search commands
  (defvar m-search-map (make-sparse-keymap))
  (general-create-definer m-search-def
    :keymaps m-all-states
    :prefix "SPC s"
    :global-prefix "C-c s"
    :prefix-map 'm-search-map
    "" (cons "search" m-search-map))

  ;; desktop (session) commands
  (defvar m-desktop-map (make-sparse-keymap))
  (general-create-definer m-desktop-def
    :keymaps m-all-states
    :prefix "SPC d"
    :global-prefix "C-c d"
    :prefix-map 'm-desktop-map
    "" (cons "desktop" m-desktop-map))

  ;;; local leader. major mode bindings belong here
  ;; (defvar m-mode-map (make-sparse-keymap))
  (general-create-definer m-mode-def
    :keymaps m-all-states
    :prefix "SPC m"
    :global-prefix "C-c m"
    ;; Disabled because this breaks binding to specific keymaps
    ;; :prefix-map 'm-mode-map
    ;; "" (cons "mode" m-mode-map)
    )

  ;;; misc leader. for commands that don't belong
  (defvar m-misc-map (make-sparse-keymap))
  (general-create-definer m-misc-def
    :keymaps m-all-states
    :prefix "SPC v"
    :global-prefix "C-c v"
    :prefix-map 'm-misc-map
    "" (cons "misc" m-misc-map)))

(defvar m-module-path (expand-file-name "lisp" user-emacs-directory))
(add-to-list 'load-path m-module-path)

;; load the scribb.el file, not stable!
(require 'scribb)

;; easy way to print stuff
(defmacro m-f (&rest msg)
  "Message format"
  `(message (format ,@msg)))

;; make it slightly easier to remove added advice
(defmacro advice-add-- (symbol _how function &optional _props)
  "Easy way to remove advice, just add --"
  `(advice-remove ,symbol ,function))

(defmacro define-advice-- (symbol args &rest _body)
  "Easy way to remove advice, just add --"
  (declare (indent 2) (doc-string 3) (debug (sexp sexp def-body)))
  `(advice-remove ',symbol #',(intern (format "%s@%s" symbol (nth 2 args)))))

(defvar m-macro-enter-hook nil)
(defvar m-macro-exit-hook nil)

(defmacro m-set-kmacro (var val &optional depth local)
  "Set value of VAR to VAL during macro recording/execution.
Optional DEPTH is used in m-macro-enter-hook and
m-macro-exit-hook. Optional LOCAL will use setq-local instead of
setq."
  `(progn
     (add-hook 'm-macro-enter-hook
               (defun ,(intern (format "m-macro-enter-%s" var)) ()
                 (setq ,(intern (format "m-macro-%s" var)) ,var)
                 (setq ,var ,val)))
     (add-hook 'm-macro-exit-hook
               (defun ,(intern (format "m-macro-exit-%s" var)) ()
                 (setq ,var ,(intern (format "m-macro-%s" var)))
                 (makunbound ',(intern (format "m-macro-%s" var)))))))

;; Now we just need to run enter/exit macro hooks when recording macros:

;; Recording macros
(define-advice kmacro-start-macro (:before (&rest _) call-macro-enter-hook)
  (run-hooks 'm-macro-enter-hook))
(define-advice end-kbd-macro (:after (&rest _) call-macro-exit-hook)
  (run-hooks 'm-macro-exit-hook))

;; When macro execution stops
(add-hook 'kbd-macro-termination-hook
          (defun m-macro-run-exit-hooks ()
            (run-hooks 'm-macro-exit-hook)))

;; When starting macro execution with Emacs commands
(define-advice kmacro-call-macro (:before (&rest _) call-macro-enter-hook)
  (run-hooks 'm-macro-enter-hook))

;; When starting macro execution with evil
(define-advice execute-kbd-macro (:before (&rest _) call-macro-enter-hook)
  (run-hooks 'm-macro-enter-hook))

(setopt custom-file (expand-file-name "~/sfs/dotfiles/emacs/lisp/custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

(use-package files
  :if t
  :demand t
  :config
  ;; taken from https://stackoverflow.com/questions/2680389/how-to-remove-all-files-ending-with-made-by-emacs
  (setopt backup-directory-alist `(("." . ,(expand-file-name "backup" user-emacs-directory)))
          backup-by-copying t    ; Don't delink hardlinks
          version-control t      ; Use version numbers on backups
          delete-old-versions t  ; Automatically delete excess backups
          kept-new-versions 20   ; how many of the newest versions to keep
          kept-old-versions 5    ; and how many of the old
          ))

(use-package vc-hooks
  :if t
  :demand t
  :config
  (setopt
   vc-make-backup-files t ; make backup files even for version controlled files
   ))

(use-package savehist
  :if t
  :demand t
  :config
  (savehist-mode 1)                     ; save minibuffer history
  )

(use-package recentf
  :if t
  :demand t
  :config
  ;; check the binding for `consult-recent-file'
  (recentf-mode 1)                      ; remember recent files
  (setopt recentf-max-saved-items 100)  ; default 20
  )

(use-package saveplace
  :if t
  :demand t
  :config
  (save-place-mode 1))                  ; save place within files

(use-package desktop
  :if t
  :demand t
  :preface
  (declare-function desktop-full-file-name "desktop")
  :config
  ;; try to load session from PWD if possible or fall back to user-emacs-directory
  (setopt desktop-path (list "." user-emacs-directory)
          desktop-restore-eager 10 ; only restore 10 buffers immediately, rest lazily
          )

  (defvar m-desktop-file-list-location (expand-file-name "desktops" user-emacs-directory)
    "File to save .emacs.desktop file locations in")

  ;; whenever saving a desktop file put it in cache if it's not already
  (add-hook 'desktop-save-hook
            (defun m-write-desktop-file (&rest args)
              (when-let ((desktop-file (desktop-full-file-name)))
                (with-temp-buffer
                  (when (file-exists-p m-desktop-file-list-location)
                    (insert-file-contents m-desktop-file-list-location))
                  (goto-char (point-min))
                  (unless (search-forward desktop-file nil t)
                    (insert (format "%s\n" (file-name-directory desktop-file)))))
                (unless desktop-save-mode
                  (desktop-save-mode +1)))))

  ;; start desktop save mode after reading a desktop file, that way it is saved there automatically
  (add-hook 'desktop-after-read-hook
            (defun m-start-desktop-save-mode ()
              (desktop-save-mode +1)))

  ;; util function for reading the desktops file for interactive commands
  (defun m-read-desktop-dirs ()
    (if (file-exists-p m-desktop-file-list-location)
        (completing-read
         "Location: "
         (mapcar #'file-name-directory
                 (string-split (with-temp-buffer
                                 (insert-file-contents m-desktop-file-list-location)
                                 (buffer-string))
                               "\n" t)))
      (user-error "No desktop files exist. Consider running M-x m-desktop-redo-file-list.")))

  (defun m-desktop-load-dir (dir)
    "Load an existing desktop directory"
    (interactive (list (m-read-desktop-dirs)))
    (desktop-change-dir dir))

  (defun m-desktop-delete-dir (dir)
    "Delete a dir from the desktops file and it's corresponding .emacs.desktop file"
    (interactive (list (m-read-desktop-dirs)))
    (delete-file (expand-file-name ".emacs.desktop" dir))
    (with-temp-buffer
      (insert-file-contents m-desktop-file-list-location)
      (flush-lines (expand-file-name ".emacs.desktop" dir))
      (write-file m-desktop-file-list-location)))

  (defun m-desktop-redo-file-list (base-dir)
    "Clean up the file list, adding .emacs.desktop files and deleting any in
there that no longer exist"
    (interactive "DBase Directory: ")
    (setq m-desktop-find-files-str ""
          m-desktop-find-buffer (get-buffer-create "*desktop-find*")
          m-desktop-find-err-buffer (get-buffer-create "*desktop-find-err*"))
    (make-process
     :name "desktop-find"
     :buffer m-desktop-find-buffer
     :stderr m-desktop-find-err-buffer
     ;; https://stackoverflow.com/questions/762348/how-can-i-exclude-all-permission-denied-messages-from-find
     :command `("find" ,(expand-file-name base-dir) "!" "-readable" "-prune" "-o" "-name" ".emacs.desktop" "-print")
     :filter (defun m-desktop-find-filter (proc str)
               (setq m-desktop-find-files-str (concat m-desktop-find-files-str str)))
     :sentinel (defun m-desktop-find-sentinel (proc status)
                 (if (string= status "finished\n")
                     (progn
                       (with-temp-file m-desktop-file-list-location
                         (delete-region (point-min) (point-max))
                         (insert m-desktop-find-files-str))
                       (when (buffer-live-p (process-buffer proc))
                         (kill-buffer (process-buffer proc)))
                       (when (buffer-live-p m-desktop-find-err-buffer)
                         (kill-buffer m-desktop-find-err-buffer))
                       (makunbound 'm-desktop-find-files-str)
                       (message "Done updating desktops"))
                   (error "Error in desktop find. see *desktop-find-err* buffer"))))))

(use-package desktop
  :if t
  :demand t
  :config
  (m-desktop-def 'normal
    "c" #'desktop-clear ; clear everything from current desktop
    "l" #'m-desktop-load-dir
    "d" #'m-desktop-delete-dir
    "r" #'m-desktop-redo-file-list
    "s" #'desktop-save ; prompts for where to save
    "f" #'desktop-change-dir ; interactively find directory
    "g" #'desktop-revert ; revert to last loaded desktop
    "m" #'desktop-save-mode ; auto enabled when reading
    ))

(use-package repeat ; built in
  :disabled ; i rarely use this feature, so disabling for now...
  :if t
  :defer 1
  :config
  (repeat-mode 1)
  (setopt repeat-exit-key "<return>"
          repeat-exit-timeout 0.3))

(use-package emacs
  :if t
  :demand t
  :config
  (setq-default buffer-file-coding-system 'utf-8-unix))

(use-package autorevert
  :if t
  :defer 1
  :config
  (global-auto-revert-mode 1))

(use-package emacs
  :if t
  :demand t
  :config
  (setq-default tab-width (if (or m-ww? m-wl?) 2 4)))

(use-package simple
  :if t
  :demand t
  :config
  (setq-default indent-tabs-mode nil)
  (define-advice yank (:after (&rest _) fix-indent)
    "Fix indentation automatically after yanking"
    (indent-region (mark) (point))))

(use-package ws-butler
  :if t
  :defer 1
  :commands (ws-butler-global-mode)
  :init
  (ws-butler-global-mode)
  :diminish
  'ws-butler-mode)

(use-package files
  :if t
  :defer 1
  :config
  (setopt require-final-newline t))

(use-package files
  :if t
  :demand t
  :config
  (m-buffer-def 'normal
    "g" #'revert-buffer-quick)
  (m-leader 'normal
    "f" #'find-file)
  (setopt revert-buffer-quick-short-answers t) ; y-or-n-p not yes-or-no-p
  )

(use-package simple
  :if t
  :demand t
  ;; :after (evil)
  :config
  (m-buffer-def 'normal
    "K" #'kill-current-buffer)
  ;; (general-def 'normal
  ;;   "g K" #'kill-current-buffer)
  )

(use-package window
  :if t
  :demand t
  ;; :after (evil)
  :config
  (m-buffer-def 'normal
    "b" #'switch-to-buffer
    "o" #'switch-to-buffer-other-window
    "k" #'bury-buffer)

  ;; bind gt and gT for general use. by default these are bound to tab-bar-mode, which i don't use
  ;; (general-def 'normal
  ;;   "g t" #'next-buffer
  ;;   "g T" #'previous-buffer
  ;;   "g k" #'bury-buffer)
  )

(use-package recentf
  :if t
  :demand t
  :config
  (m-buffer-def 'normal
    "r" #'recentf))

(use-package epa-file
  :unless (or m-wl? m-ww?)
  :defer 2
  :init
  (epa-file-enable)
  :config

  ;; If t, always ask user to select recipients.
  ;; If nil, query user only when `epa-file-encrypt-to' is not set.
  ;; If neither t nor nil, don't ask user.  In this case, symmetric
  ;; encryption is used."
  (setopt epa-file-select-keys nil))

(use-package epg-config
  :unless (or m-wl? m-ww?)
  :defer 2
  :init
  (setopt epg-pinentry-mode 'loopback))

(use-package pinentry
  :unless (or m-wl? m-ww?)
  :defer 2
  :config
  (pinentry-start))


(use-package epa-hook
  :unless (or m-wl? m-ww?)
  :defer 2
  :config
  (setopt
   ;; XXX this should probably go in .dir-locals.el
   epa-file-encrypt-to "Marcus Quincy"))

(use-package sudo-edit
  :if t
  :defer 3)

(use-package persistent-scratch
  :if t
  :defer 1 ; demand t breaks a bunch of other things for some reason
  :commands (persistent-scratch-setup-default)
  :config
  ;; save scratch buffer whenever I've been idle for X seconds
  (setopt persistent-scratch-autosave-interval '(idle . 15.0))
  ;; enable loading and saving
  (persistent-scratch-setup-default))

(use-package emacs
  :if t
  :defer 1
  :config
  (m-buffer-def '(normal visual emacs insert)
    "s" (defun m-open-scratch-buffer ()
          (interactive)
          (display-buffer (get-buffer-create "*scratch*")))
    ;; XXX this clashes with switch-to-minibuffer. Just use "SPC h e" instead
    ;; "M" (defun m-open-messages-buffer ()
    ;;       (interactive)
    ;;       (display-buffer (get-buffer-create "*Messages*")))
    ))

(use-package files
  :if t
  :demand t
  :config
  (setopt confirm-kill-emacs #'y-or-n-p))

(use-package wgrep
  :if t
  :defer 3)

(use-package emacs ;paragraphs
  :if t
  :defer 2
  :init
  (setopt sentence-end-double-space nil))

(use-package emacs
  :if t
  :defer 1
  :config
  (setopt echo-keystrokes .1))

(use-package emacs
  :if t
  :defer 3
  :preface
  (declare-function evil-normal-state "evil")
  (declare-function evil-normal-state-p "evil")
  (declare-function evil-append "evil")
  :config
  (defun m-eval-and-replace ()
    "Replace the preceding sexp with its value."
    (interactive)
    (with-syntax-table lisp-mode-syntax-table
      (backward-kill-sexp)
      (condition-case nil
          (prin1 (eval (read (current-kill 0)))
                 (current-buffer))
        (error (message "Invalid expression")
               (insert (current-kill 0))))))

  (defun m-eval-and-replace-region (beg end)
    (interactive "r")
    (let* ((sexps (buffer-substring-no-properties beg end))
           (result
            (with-temp-buffer
              (with-syntax-table lisp-mode-syntax-table
                (insert sexps)
                (goto-char (point-min))
                (while (not (eq (point) (progn (forward-sexp) (point))))
                  (m-eval-and-replace)))
              (buffer-substring-no-properties (point-min) (point-max)))))
      (kill-region beg end)
      (insert result)))

  (defun m-eval-and-replace-dwim ()
    (interactive)
    (if (region-active-p)
        (call-interactively #'m-eval-and-replace-region)
      (if (and (fboundp #'evil-normal-state-p) (evil-normal-state-p))
          (progn
            (call-interactively #'evil-append)
            (call-interactively #'m-eval-and-replace)
            (call-interactively #'evil-normal-state))
        (call-interactively #'m-eval-and-replace))))

  (general-def global-map
    "C-x C-y" #'m-eval-and-replace-dwim)
  (m-misc-def '(normal insert visual emacs)
    "e" #'m-eval-and-replace-dwim))

(use-package emacs
  :if m-wl?
  :defer 3
  :config
  ;; https://www.fredgruber.org/post/wsl_emacs_clipboard/
  (defun m-wsl-copy-clip(&rest _args)
    "Copies the current kill to windows clipboard"
    (interactive)
    (let ((mytemp (make-temp-file "winclip")))
      (write-region
       (current-kill 0 t)
       nil
       mytemp)
      (shell-command (concat "clip.exe<" mytemp ))
      (delete-file mytemp)))

  (m-misc-def '(normal emacs)
    "w" #'m-wsl-copy-clip))

(use-package evil-collection
  :disabled
  :if t
  :demand t
  :after (evil)
  :commands (evil-collection-consult-mark)
  :config
  (m-buffer-def '(normal emacs visual insert)
    "m" #'evil-collection-consult-mark))

;; use this in place because using Emacs registers
(use-package consult
  :if t
  :demand t
  :commands (consult-register)
  :config
  (m-buffer-def '(normal emacs visual insert)
    "m" #'consult-register))

(use-package register
  :if t
  :demand t
  :after (evil)
  :commands (evil-set-marker
             evil-goto-mark-line
             evil-goto-mark)
  :config
  (defun m-evil-set-marker (char)
    "Like evil-set-marker, but if CHAR is capital sets it as an Emacs
register in addition to evil mark."
    (interactive "c")
    (evil-set-marker char)
    (when (<= ?A char ?Z)
      (point-to-register char)))

  (defun m-evil-goto-mark-line (char)
    "Like evil-goto-mark-line, but if CHAR is a capital letter uses
Emacs register instead of evil."
    (interactive "c")
    (if (<= ?A char ?Z)
        (progn
          (jump-to-register char)
          (back-to-indentation))
      (evil-goto-mark-line char)))

  (defun m-evil-goto-mark (char)
    "Like evil-goto-mark, but if CHAR is a capital letter uses
Emacs register instead of evil."
    (interactive "c")
    (if (<= ?A char ?Z)
        (jump-to-register char)
      (evil-goto-mark char)))

  (general-def '(normal visual)
    "m" #'m-evil-set-marker
    "'" #'m-evil-goto-mark-line
    "`" #'m-evil-goto-mark))

(use-package evil-fringe-mark
  :if t
  :demand t
  :after (evil)
  :commands (global-evil-fringe-mark-mode
             evil-fringe-mark-mode)
  :preface
  (declare-function -difference "dash")
  (declare-function evil-set-marker "evil")
  :init
  (setopt
   ;; show on rhs or lhs
   evil-fringe-mark-side 'left-fringe
   evil-fringe-mark-margin 'left-margin
   ;; Display special marks (like <>, [], {}, ^)
   evil-fringe-mark-show-special t)
  :config
  ;; paragraph marks are interesting. They show how navigation via {
  ;; and } will work. But, they sometimes show up in buffers where
  ;; they shouldn't. I will disable them by default, but allow them
  ;; where they seem to work OK
  (add-to-list 'evil-fringe-mark-ignore-chars ?{)
  (add-to-list 'evil-fringe-mark-ignore-chars ?})

  (defun m-local-enable-evil-fringe-paragraph-marks ()
    (require 'dash)
    (setq-local evil-fringe-mark-ignore-chars (-difference evil-fringe-mark-ignore-chars '(?{ ?}))))

  (add-hook 'prog-mode-hook #'m-local-enable-evil-fringe-paragraph-marks)
  ;; (add-hook 'text-mode-hook #'m-local-enable-evil-fringe-paragraph-marks)

  ;; only enable in certain modes. This seems to fuck up in the echo area
  (defun m-enable-evil-fringe-mark-mode ()
    "Enable evil-fringe-mark mode after idle timer delay. This helps
with clashes with git-gutter and making sure it has marks all set."
    ;; (run-with-idle-timer 1 nil
    ;;                      (lambda (buffer)
    ;;                        (when (buffer-live-p buffer)
    ;;                          (with-current-buffer buffer
    ;;                            (evil-fringe-mark-mode +1))))
    ;;                      (current-buffer)))
    (evil-fringe-mark-mode +1)
    ;; go through all Emacs registers and "register" them with evil-fringe-mark-mode
    (seq-do (lambda (register-cons)
              ;; (m-f "reg cons %s" register-cons)
              (when (<= ?A (car register-cons) ?Z)
                (cond
                 ((and (markerp (cdr register-cons))
                       (eq (marker-buffer (cdr register-cons)) (current-buffer)))
                  (evil-set-marker (car register-cons) (marker-position (cdr register-cons))))
                 ((and (consp (cdr register-cons))
                       (eq (cadr register-cons) 'file-query)
                       (string= (caddr register-cons) (buffer-file-name (current-buffer))))
                  (evil-set-marker (car register-cons) (cadddr register-cons))))))
            ;; (m-f "setting evil marker %c to %s" (car register-cons) (marker-position (cdr register-cons)))
            register-alist))
  (add-hook 'prog-mode-hook #'m-enable-evil-fringe-mark-mode +10)
  (add-hook 'text-mode-hook #'m-enable-evil-fringe-mark-mode +10))

(use-package desktop
  :if t
  :demand t
  ;; :after (evil)
  :config
  ;;; Old Evil setup:
  ;; (defvar evil-markers-alist) ; silence warnings

  ;; ;; save file local marks.
  ;; (add-to-list 'desktop-locals-to-save 'evil-markers-alist)
  ;; ;; save global marks.
  ;; (add-to-list 'desktop-globals-to-save 'evil-markers-alist)

  ;; ;; HACK
  ;; (add-hook 'desktop-after-read-hook
  ;;           (defun m-prepare-evil-markers()
  ;;             (setq-default evil-markers-alist evil-markers-alist)
  ;;             (kill-local-variable 'evil-markers-alist)
  ;;             (make-local-variable 'evil-markers-alist)))

  ;;; New Emacs setup:
  (add-to-list 'desktop-globals-to-save 'register-alist))

;; NOTE: configuration originally written with modus-themes 4.2.0
(use-package modus-themes
  :if t
  :demand t
  :config
  ;; Add all your customizations prior to loading the themes
  (setopt modus-themes-custom-auto-reload t ; automatically reload the theme when changing with customize (or setopt!)
          modus-themes-italic-constructs t ; use more italics
          modus-themes-bold-constructs t ; use more bolds in code
          modus-themes-disable-other-themes t ; disable other themes when loading modus
          modus-themes-mixed-fonts t ; helps ensure that things like code blocks and tables stay monospaced even when variable pitch is on
          modus-themes-prompts '(semibold) ; used, for example, in minibuffer prompts
          modus-themes-completions '((matches . (bold))
                                     (selection . (regular))) ; need to make selection regular so bold shows up in matches
          modus-themes-org-blocks 'tinted-background ; tint org src block backgrounds based on language
          modus-themes-headings '((1 . (1.2))
                                  (2 . (1.1))
                                  (t . (1.05))) ; controls size and bolding of headings
          modus-themes-variable-pitch-ui nil ; don't use variable pitch font, i.e. in modeline
          modus-themes-common-palette-overrides
          `(
            ;; i want a colored modeline
            (bg-mode-line-active bg-blue-nuanced)
            (fg-mode-line-active fg-main)
            (border-mode-line-active blue-faint)

	    ;; NOTE: info section 5.2.3 for coloring tab bar/line, if i decide to use it

	    ;; make the fringe not colorful
	    (fringe unspecified)

	    ;; keep a underline for links so it's obvious
	    (underline-link border)
	    (underline-link-visited border)
	    (underline-link-symbolic border)

	    ;; for now we randomly change colors up a little bit, based on examples in info page
	    ,@(pcase (seq-random-elt '(night summer bio trio-light))
		('night '((builtin green-cooler)
			  (comment fg-dim) ; was yellow-faint
			  (constant magenta-cooler)
			  (fnname cyan-cooler)
			  (keyword blue-warmer)
			  (preprocessor red-warmer)
			  (docstring cyan-faint)
			  (string blue-cooler)
			  (type magenta-cooler)
			  (variable cyan)))
		('summer '((builtin magenta)
			   (comment fg-dim) ; was yellow-faint
			   (constant red-cooler)
			   (fnname magenta-warmer)
			   (keyword magenta-cooler)
			   (preprocessor green-warmer)
			   (docstring cyan-faint)
			   (string yellow-warmer)
			   (type cyan-warmer)
			   (variable blue-warmer)))
		('bio '((builtin green)
			(comment fg-dim) ; was yellow-faint
			(constant blue)
			(fnname green-warmer)
			(keyword green-cooler)
			(preprocessor green)
			(docstring green-faint)
			(string magenta-cooler)
			(type cyan-warmer)
			(variable blue-warmer)))
		('trio-light '((builtin magenta-cooler)
			       (comment fg-dim) ; was yellow-faint
			       (constant magenta-warmer)
			       (fnname blue-warmer)
			       (keyword magenta)
			       (preprocessor red-cooler)
			       (docstring magenta-faint)
			       (string green-cooler)
			       (type cyan-cooler)
			       (variable cyan-warmer))))

	    (bg-paren-match bg-cyan-subtle)
	    (underline-paren-match blue-faint)

	    ;; pull in *faint* for defaults (options are faint, intense, warmer, cooler, or nil for default)
	    ,@modus-themes-preset-overrides-faint))

  ;; Load the theme of your choice.
  (load-theme 'modus-vivendi t))

(use-package nerd-icons
  :if (not (or m-ww?))
  :demand t
  :commands (nerd-icons-install-fonts)
  :init
  ;; The Nerd Font you want to use in GUI
  ;; "Symbols Nerd Font Mono" is the default and is recommended
  ;; but you can use any other Nerd Font if you want

  ;; XXX on m-wl install a nerd font from the nerd fonts website and manually set terminal to use it
  (setopt nerd-icons-font-family "Symbols Nerd Font Mono")
  :config
  ;; install the fonts if they don't exist yet
  (unless (or m-wl? (file-exists-p (concat (getenv "HOME") "/.local/share/fonts/NFM.ttf")))
    (call-interactively #'nerd-icons-install-fonts)))

(use-package doom-modeline
  :if t
  :demand t
  :commands (doom-modeline-mode)
  :init
  ;; make it so doom modeline works correctly even in emacsclient
  (when (daemonp)
    (add-hook 'after-make-frame-functions
              (defun m-after-frame-set-doom-modeline (frame)
                (with-selected-frame frame
                  ;; TODO test this
                  (setopt doom-modeline-icon doom-modeline-icon)))))

  (setopt
   ;; use icons if font exists
   doom-modeline-icon (or m-wl? (file-exists-p (concat (getenv "HOME") "/.local/share/fonts/NFM.ttf")))
   ;; show which letter an evil kbd macro is being saved to
   doom-modeline-always-show-macro-register t
   ;; show encoding if it's not LF utf8
   doom-modeline-buffer-encoding 'nondefault
   ;; adjust how buffer name is show. 'auto is also good
   doom-modeline-buffer-file-name-style 'truncate-all
   ;; show the word count in visual mode
   ;; doom-modeline-enable-word-count t
   ;; NOTE: when GNUs set up enable this
   ;; doom-modeline-gnus t
   ;; show line indicating position in buffer instead of just a bar (gui only)
   doom-modeline-hud t
   ;; show mu4e unread mail notificationss
   doom-modeline-mu4e t
   )

  :config
  (doom-modeline-mode +1))

(use-package evil-anzu
  :if t
  :defer 3
  :commands (global-anzu-mode)
  :config
  (global-anzu-mode 1))

;; TODO checkout Prot's lin package
(use-package hl-line
  :when t
  :defer 1
  :config
  (global-hl-line-mode))

(use-package emacs
  :disabled
  :config
  (when (version< "29" emacs-version)
    ;; true background transparency!
    ;; (set-frame-parameter nil 'alpha-background 0.75)
    (add-to-list 'default-frame-alist '(alpha-background . 0.75))))

(use-package display-line-numbers
  :if t
  :defer 1
  :config
  ;; relative line numbers
  (setq-default display-line-numbers 'visual)
  ;; disable line numbers in certain modes
  (require 'seq)
  (seq-do (lambda (hook) (add-hook hook
                                   (defun m-disable-line-numbers ()
                                     (display-line-numbers-mode 0))))
          '(magit-mode-hook
            compilation-mode-hook
            term-mode-hook
            shell-mode-hook
            eshell-mode-hook
            vterm-mode-hook))

  ;; create keybinding to cycle line numbers
  (m-toggle-def 'normal
    "l" (defun m-cycle-line-numbers ()
          (interactive)
          (setopt display-line-numbers
                  (pcase display-line-numbers
                    ('visual nil)
                    ('t 'visual)
                    ('nil t))))))

(use-package faces
  :if t
  :demand t
  :config
  ;; fonts
  (defun m-set-font-faces (&optional font)
    (let ((font (or font "Fira Code"))
          (mono-height 120))
      (set-face-attribute 'default nil :font font :height mono-height)
      (set-face-attribute 'fixed-pitch nil :font font :height mono-height)
      (set-face-attribute 'variable-pitch nil :height 160)))

  ;; used so font is right in emacs client
  (if (daemonp)
      (add-hook 'after-make-frame-functions
                (defun m-after-frame-set-font (frame)
                  (with-selected-frame frame
                    (m-set-font-faces))))
    (m-set-font-faces)))

(use-package simple
  :if t
  :defer 1
  :config
  (m-toggle-def 'normal
    "v" #'visual-line-mode)

  ;; add indicators for visual line mode
  (setopt visual-line-fringe-indicators '(left-curly-arrow nil)) ; doesn't seem to be working in terminal emacs...

  ;; show column numbers in modeline in addition to line numbers
  (column-number-mode +1))

(use-package rainbow-delimiters
  :if t
  :defer t
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package rainbow-mode
  :if t
  :defer t
  :diminish rainbow-mode
  :hook (text-mode prog-mode))

(use-package which-key
  :if t
  :demand t
  :after (evil) ; we need evil loaded first so which key works with it.
  :commands (which-key-mode
             which-key-setup-minibuffer
             which-key-show-top-level
             which-key-show-major-mode)
  :diminish which-key-mode
  :init (which-key-mode)
  :config
  ;; prefer popping up on the right, but use bottom if there's no room
  ;; (which-key-setup-side-window-right-bottom)
  ;; or in the minibuffer
  (which-key-setup-minibuffer)
  (setopt which-key-idle-delay 0.4     ; seconds before showing
	  which-key-allow-evil-operators t    ; integrate with evil
	  )
  (general-def m-help-map
    "T" #'which-key-show-top-level
    "M" #'which-key-show-major-mode))

(use-package helpful
  :if t
  :defer 1
  :commands (helpful-kill-buffers)
  :bind
  ([remap describe-function] . helpful-callable) ; helpful-callable does functions and macros
  ([remap describe-variable] . helpful-variable)
  ([remap describe-symbol] . helpful-symbol)
  ([remap describe-command] . helpful-command)
  ([remap describe-key] . helpful-key)
  ("C-h ." . helpful-at-point)
  :config
  (setopt helpful-max-buffers 3)
  (m-mode-def 'normal helpful-mode-map
    "k" #'helpful-kill-buffers))

(use-package emacs
  :if t
  :demand t
  :commands (scroll-bar-mode)
  :config
  ;; removing ugly things
  (setopt visible-bell t)
  (when (display-graphic-p)
    (scroll-bar-mode -1)        ; Disable visible scrollbar
    ))

(use-package menu-bar
  :if t
  :demand t
  :config
  (menu-bar-mode -1)          ; Disable the menu bar
  )

(use-package tooltip
  :if t
  :demand t
  :config
  (tooltip-mode -1)           ; Make tooltips show in echo area (not in buffer)
  )

;; intentionally outside use-package
(setopt inhibit-startup-message t)

(use-package tool-bar
  :if t
  :demand t
  :config
  (when (display-graphic-p)
    (tool-bar-mode -1))          ; Disable the toolbar
  )

(use-package tab-line
  :disabled
  :if t
  :demand t
  :after (magit)
  :commands (global-tab-line-mode
             tab-line-select-tab-buffer
             tab-line-tabs-window-buffers)
  :preface
  (declare-function -elem-index "dash")
  :config
  (setopt tab-line-close-button-show nil ; don't show close button
          tab-line-new-button-show nil ; don't show new button
          tab-line-switch-cycling t ; only consulted if tab-line-tabs-function is not tab-line-tabs-window-buffers
          ;; tab-line-tabs-function 'tab-line-tabs-buffer-groups
          )
  (defun m-select-tab-bar-buffer (num)
    "Select a buffer in tab-line-mode by number"
    (interactive (list (or current-prefix-arg (read-number "buffer #: "))))
    (ignore-errors
      (tab-line-select-tab-buffer (nth num (tab-line-tabs-window-buffers)))))

  ;; define g# in evil to easily navigate tabs
  (dolist (num (number-sequence 0 9))
    (eval `(progn
             (general-def '(normal)
               ,(format "g %d" num)
               (defun ,(intern (format "m-switch-to-tab-%s" num)) ()
                 (interactive)
                 (m-select-tab-bar-buffer ,num)))
             ;; and make it work from magit too (note: gt and gT won't work, but whatever...)
             (general-def 'normal magit-mode-map
               ,(format "g %d" num)
               (defun ,(intern (format "m-switch-to-tab-%s" num)) ()
                 (interactive)
                 (m-select-tab-bar-buffer ,num))))))

  ;; make a custom function for how tabs are named, with number labels and truncation
  (setopt tab-line-tab-name-function
          (defun m-name-buffer-tab (buffer buffers)
            (require 'dash)
            (let ((tab-name (concat
                             (when (window-dedicated-p (selected-window))
                               "P")
                             (format "%d" (-elem-index buffer buffers))
                             ". "
                             (buffer-name buffer))))
              (if (< (length tab-name) tab-line-tab-name-truncated-max)
                  tab-name
                (propertize
                 (concat (substring tab-name 0 (- tab-line-tab-name-truncated-max 4))
                         "⋯"
                         (substring tab-name (- (length tab-name) 4)))
                 'help-echo tab-name)))))

  (global-tab-line-mode +1))

(use-package hl-todo
  :if t
  :defer 1
  :commands (global-hl-todo-mode)
  :config
  (global-hl-todo-mode 1)
  (defun m-modus-themes-hl-todo-faces ()
    (add-to-list 'hl-todo-keyword-faces
                 (cons "IDEA" "#6ae4b9")))
  (m-modus-themes-hl-todo-faces) ; call it once, so we don't rely on modus
  (add-hook 'modus-themes-after-load-theme-hook #'m-modus-themes-hl-todo-faces))

;; integrate with consult
(use-package consult-todo
  :init
  (m-search-def '(normal visual emacs)
    "t" #'consult-todo
    "T" #'consult-todo-all)
  (setopt consult-todo--narrow
          '((?t . "TODO")
            (?f . "FIXME")
            (?b . "BUG")
            (?h . "HACK")
            (?x . "XXX"))))

(use-package window
  :if t
  :demand t
  :after (evil)
  :config
  (general-def global-map
    "M-o" #'other-window
    "C-M-o" (defun m-backwards-other-window (count)
              (interactive "p")
              (other-window (- (or count 1)))))
  ;; unbind window bindings of habits i wanna break
  (general-def evil-window-map
    "h" nil
    "j" nil
    "k" nil
    "l" nil)
  (general-def global-map
    "C-x o" nil))

(use-package window
  :if t
  :demand t
  :config
  (advice-add 'other-window :before
              (defun m-other-window-split-if-single (&rest _)
                "Split the frame if there is a single window."
                (when (one-window-p) (split-window-sensibly))))

  (defun m-do-other-window (keys)
    ;; benefit of this is it will open a new window if currently only one (fixed by above advice!)
    ;; (call-interactively #'other-window-prefix)
    ;; benefit of this is it will show consult previews in right place
    (call-interactively #'other-window)

    ;; https://stackoverflow.com/questions/24914202/elisp-call-keymap-from-code
    (setq unread-command-events
          (mapcar (lambda (e) `(t . ,e))
                  ;; NOTE: depends on key sequence being SPC b, probably better to get from var/func
                  (listify-key-sequence (kbd keys)))))

  (general-def 'normal
    ;; NOTE: only works with evil SPC prefix, not C-c B
    "SPC B" (defun m-buffer-other-window ()
              (interactive)
              (m-do-other-window "SPC b"))
    "SPC P" (defun m-project-other-window ()
              (interactive)
              (m-do-other-window "SPC p"))
    "SPC S" (defun m-search-other-window ()
              (interactive)
              (m-do-other-window "SPC s"))
    "SPC H" (defun m-help-other-window ()
              (interactive)
              (m-do-other-window "SPC h"))))

(use-package window
  :if t
  :demand t
  :after (evil)
  :preface
  (declare-function evil-scroll-page-down "evil")
  (declare-function evil-scroll-page-up "evil")
  (declare-function evil-scroll-down "evil")
  (declare-function evil-scroll-up "evil")
  (declare-function evil-scroll-line-down "evil")
  (declare-function evil-scroll-line-up "evil")
  :config
  (general-def '(normal visual)
    "C-M-f" (defun m-scroll-page-down-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-page-down)))
    "C-M-b" (defun m-scroll-page-up-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-page-up)))
    "C-M-d" (defun m-scroll-down-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-down)))
    "C-M-u" (defun m-scroll-up-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-up)))
    "C-M-e" (defun m-scroll-line-down-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-line-down)))
    "C-M-y" (defun m-scroll-line-up-other-window ()
              (interactive)
              (with-selected-window (other-window-for-scrolling)
                (call-interactively #'evil-scroll-line-up)))))

(use-package vertico
  :if t
  :demand t
  :config
  (general-def vertico-map
    "C-M-f" #'m-scroll-page-down-other-window
    "C-M-b" #'m-scroll-page-up-other-window
    "C-M-d" #'m-scroll-down-other-window
    "C-M-u" #'m-scroll-up-other-window
    "C-M-e" #'m-scroll-line-down-other-window
    "C-M-y" #'m-scroll-line-up-other-window))

(use-package winner
  :if t
  :demand t
  :after (evil)
  :preface
  :commands (winner-undo winner-redo)
  :config
  ;; by default winner binds to C-c left and C-c right
  (setopt winner-dont-bind-my-keys t)
  (general-def evil-window-map
    "u" #'winner-undo
    "U" #'winner-redo)
  (winner-mode +1))

(use-package popper
  :if t
  :defer 1
  :commands (popper-mode
             popper-echo-mode
             popper-toggle-latest
             popper-toggle-type
             popper-cycle
             popper-group-by-project)
  :init
  ;; determines which buffers are popups
  (setopt popper-reference-buffers
          '("\\*Messages\\*"
            "Output\\*$"
            "\\*Async Shell Command\\*"
            help-mode
            helpful-mode
            compilation-mode
            ;; Info-mode

            "^\\*.*eshell.*\\*$"
            eshell-mode

            ;; trying this out... not sure if keep
            ;; "^magit"
            ;; magit-mode
            "^\\*scratch\\*$"
            ))

  ;; HACK opening eshell for first time seems busted but this fixes it
  (add-hook 'eshell-mode-hook
            (defun m-eshell-make-popper-work ()
              (popper-toggle-type (current-buffer))))

  :config
  ;; (m-toggle-def '(normal visual emacs insert)
  ;;   "p" #'popper-toggle-latest)

  (m-buffer-def '(normal visual emacs insert)
    "p" #'popper-toggle-latest
    "P" #'popper-toggle-type
    "c" #'popper-cycle)

  (popper-mode 1)

  ;; NOTE can customize keys used by this using popper-echo-dispatch-keys
  (popper-echo-mode -1))

(use-package popper
  :if t
  :defer 1
  :config
  (defvar m-popper-heights nil)
  (setopt
   popper-window-height
   (lambda (win)
     (fit-window-to-buffer
      win
      (floor (frame-height) 3)
      (alist-get (buffer-name (window-buffer win)) m-popper-heights))))

  ;; buffer local hook takes window as arg
  (add-hook 'popper-open-popup-hook
            (defun m-setup-popper-hooks ()
              (add-hook 'window-size-change-functions
                        (defun m-remember-popper-window-heights (win)
                          (when (popper-popup-p (window-buffer win))
                            (setf (alist-get (buffer-name (window-buffer win)) m-popper-heights)
                                  (window-height win))))
                        nil
                        t))))

(use-package window
  :if t
  :demand t
  :config
  (m-toggle-def 'normal
    "p" (defun m-toggle-window-dedication () ; taken from mastering emacs
          "Toggles window dedication in the selected window."
          (interactive)
          (let ((dedicated (not (window-dedicated-p (selected-window)))))
            (message "setting to dedicated: %s" dedicated)
            (set-window-dedicated-p (selected-window) dedicated))))

  (setopt
   ;; manually switching buffers should obey rules (not just programatic)
   switch-to-buffer-obey-display-actions t
   ;; try to open buffer in new window if current window is dedicated
   switch-to-buffer-in-dedicated-window 'pop)

  (progn
    (setq display-buffer-alist '())

    ;; make org src buffers replace the org buffer by default and make them dedicated
    ;; XXX org (incorrectly) uses switch-to-buffer. so getting this to work may not be possible?
    (add-to-list 'display-buffer-alist
                 '("\\*Org Src.*\\*"
                   (display-buffer-reuse-mode-window
                    display-buffer-reuse-window
                    display-buffer-same-window)
                   (dedicated . t)
                   (mode org-mode)
                   ;; (inhibit-same-window . nil)
                   ))

    ;; TODO (maybe) make it so we try to reuse major modes for programming i.e. .py will reuse prog-mode window when possible
    ))

(use-package pulse
  :if t
  :defer 3
  :config
  (defun m-pulse-line (&rest _)
    "Pulse the current line."
    (pulse-momentary-highlight-one-line (point)))

  (dolist (func '(scroll-up-command
                  scroll-down-command
                  recenter-top-bottom
                  other-window
                  ;; TODO make sure focus in works in gui
                  handle-focus-in))
    (advice-add func :after #'m-pulse-line)))

(use-package vertico
  :if t
  :demand t
  :commands (vertico-mode
             vertico-exit-input)
  :config
  (vertico-mode)
  (setopt vertico-scroll-margin 1 ; smaller scroll margin
          vertico-count 15 ; how many completion candidates to show
          vertico-resize 'grow-only ; don't shrink only grow to prevent "jitter"
          vertico-cycle t ; cycle at begin and end of completions with C-n and C-p
          )
  (general-def vertico-map
    ;; C-u RET also works...
    "C-c RET" #'vertico-exit-input))

(use-package simple
  :if t
  :defer t
  :commands (switch-to-minibuffer)
  :init
  (m-buffer-def 'normal
    "M" #'switch-to-minibuffer))

;; A few more useful configurations...
(use-package crm
  :if t
  :demand t
  :config
  ;; Support opening new minibuffers from inside existing minibuffers.
  (setopt enable-recursive-minibuffers t)
  ;; Emacs 28 and newer: Hide commands in M-x which do not work in the current
  ;; mode.  Vertico commands are hidden in normal buffers. This setting is
  ;; useful beyond Vertico.
  ;; DISABLED for now because it seems like it has issues
  ;; (setopt read-extended-command-predicate #'command-completion-default-include-p)
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode))

(use-package vertico
  :if t
  :defer 1
  :config
  ;; https://github.com/minad/vertico/wiki#pre-select-previous-directory-when-entering-parent-directory-from-within-find-file
  (defvar m-previous-directory nil
    "The directory that was just left. It is set when leaving a directory and
    set back to nil once it is used in the parent directory.")

  (defun m-set-previous-directory (&rest _)
    "Set the directory that was just exited from within find-file."
    (when (< (minibuffer-prompt-end) (point))
      (save-excursion
        (goto-char (1- (point)))
        (when (search-backward "/" (minibuffer-prompt-end) t)
          ;; set parent directory
          (setq m-previous-directory (buffer-substring (1+ (point)) (point-max)))
          ;; set back to nil if not sorting by directories or what was deleted is not a directory
          (when (not (string-suffix-p "/" m-previous-directory))
            (setq m-previous-directory nil))
          t))))

  (advice-add #'vertico-directory-up :before #'m-set-previous-directory)

  (define-advice vertico--update (:after (&rest _) choose-candidate)
    "Pick the previous directory rather than the prompt after updating candidates."
    (cond
     (m-previous-directory ; select previous directory
      (setq vertico--index (or (seq-position vertico--candidates m-previous-directory)
                               vertico--index))
      (setq m-previous-directory nil)))))

(use-package vertico-directory
  :if t
  :defer t
  :commands (vertico-directory-up
             vertico-directory-delete-char)
  :init
  (general-def vertico-map
    "DEL" #'vertico-directory-delete-char)
  )

(use-package vertico
  :if t
  :defer 3
  :commands (vertico-mode)
  :config
  (add-hook 'm-macro-enter-hook
            (defun m-disable-vertico-globally ()
              (vertico-mode -1)))
  (add-hook 'm-macro-exit-hook
            (defun m-enable-vertico-globally ()
              (vertico-mode +1))))

(use-package orderless
  :if t
  :demand t
  :config
  (setopt
   completion-category-defaults nil ; set to nil to prefer orderless always
   completion-category-overrides '((file (styles basic partial-completion))) ; makes tramp completion work

   orderless-smart-case t ; case ignored unless relevant
   ;; orderless-matching-styles '(orderless-literal orderless-regexp) ; match literal then regexp
   orderless-style-dispatchers '(orderless-affix-dispatch) ; see value of 'orderless-affix-dispatch
   )
  (setopt
   completion-styles '(orderless basic) ; prefer orderless completion but fall back to basic
   ))

;; Enable rich annotations using the Marginalia package
(use-package marginalia
  :if t
  :demand t
  :commands (marginalia-mode)
  ;; Bind `marginalia-cycle' locally in the minibuffer.  To make the binding
  ;; available in the *Completions* buffer, add it to the
  ;; `completion-list-mode-map'.
  :bind (:map minibuffer-local-map
         ("M-A" . marginalia-cycle)) ; changes how much marginalia stuff visible

  ;; The :init section is always executed.
  :config

  ;; Marginalia must be activated in the :init section of use-package such that
  ;; the mode gets enabled right away. Note that this forces loading the
  ;; package.
  (marginalia-mode))

(use-package consult
  :if t
  :defer 1
  :preface
  (declare-function consult--buffer-state "consult")
  (declare-function consult--buffer-query "consult")
  (declare-function consult--buffer-pair "consult")
  :commands (consult-buffer
             consult-buffer-other-window
             consult-buffer-other-frame
             consult-project-buffer
             consult-recent-file
             consult-yank-pop
             consult-goto-line
             consult-apropos
             consult-complex-command
             consult-org-heading
             consult-imenu
             consult-imenu-multi
             consult-line
             consult-line-multi
             consult-git-grep
             consult-mode-command
             consult-info
             consult-find)
  :init
  (general-def global-map
    ;; common
    [remap switch-to-buffer] #'consult-buffer
    [remap switch-to-buffer-other-window] #'consult-buffer-other-window
    [remap switch-to-buffer-other-frame] #'consult-buffer-other-frame
    [remap project-switch-to-buffer] #'consult-project-buffer
    [remap recentf] #'consult-recent-file

    ;; less common, but may as well
    [remap yank-pop] #'consult-yank-pop
    [remap goto-line] #'consult-goto-line
    [remap apropos-command] #'consult-apropos
    [remap repeat-complex-command] #'consult-complex-command)
  (general-def '(normal visual emacs)
    "M-y" #'consult-yank-pop)
  (m-search-def '(normal visual emacs)
    "i" (defun m-consult-imenu ()
          (interactive)
          (if (eq major-mode 'org-mode)
              (call-interactively #'consult-org-heading)
            (call-interactively #'consult-imenu)))
    "I" #'consult-imenu-multi
    "s" #'consult-line
    "S" #'consult-line-multi
    "g" #'consult-git-grep
    "x" #'consult-complex-command ; shows recently executed commands
    "c" #'consult-mode-command ; shows all major/minor mode commands
    "n" #'consult-info)
  (m-buffer-def '(normal visual emacs)
    "f" #'consult-find)

  :config
  (setopt consult-async-min-input 2 ; be a bit more agressive about starting async calls (default 3)
          consult-fontify-max-size (* 100 1000) ; default 1MB
          )

  ;; needed so consult knows about my wrapper command
  ;; see https://github.com/minad/consult?tab=readme-ov-file#live-previews
  (consult-customize m-consult-imenu)

  (setopt consult-preview-excluded-files
          '("\\`/[^/|:]+:" ; remote files
            ".gpg$" ; encrypted files
            ))

  ;; redefine consult--source-buffer, but with predicate. If i ever want to add a predicate
  ;; to what buffers appear in consult-buffer, I can add it to the m-consult-buffer-predicates
  (defvar m-consult-buffer-predicates '()
    "List of predicates each taking a buffer as an argument and
returning t if the buffer should be included in the consult-buffer
list and nil otherwise")

  ;; this implementation is the same as what's in consult.el, just with predicate added to the query
  (setq consult--source-buffer
        `(:name     "Buffer"
                    :narrow   ?b
                    :category buffer
                    :face     consult-buffer
                    :history  buffer-name-history
                    :state    ,#'consult--buffer-state
                    :default  t
                    :items
                    ,(lambda () (consult--buffer-query
                                 :predicate (lambda (buffer)
                                              (eval `(and ,@(mapcar (lambda (pred)
                                                                      (funcall pred buffer))
                                                                    m-consult-buffer-predicates))))
                                 :sort 'visibility
                                 :as #'consult--buffer-pair)))))

(use-package consult
  :if t
  :defer 1
  :commands (consult-ripgrep)
  :preface
  (declare-function vertico--candidate "vertico")
  :init
  (m-search-def '(normal visual emacs)
    "r" #'consult-ripgrep
    "R" (defun m-cr-same-extension (ext)
          (interactive
           (list (let ((file (buffer-file-name (current-buffer))))
                   (if file
                       (file-name-extension file)
                     (read-string "Extension: ")))))
          ;; XXX: minad says this may not work consistently... see https://github.com/minad/consult/wiki#temporarily-override-consult-ripgrep-args
          ;; will fix if breaks later
          (let ((consult-ripgrep-args (concat consult-ripgrep-args
                                              (format " -g \"*.%s\"" ext))))
            (call-interactively #'consult-ripgrep))))

  :config
  (require 'vertico)
  (defvar m-consult-ripgrep-map (make-sparse-keymap))

  ;; add keymaps
  (consult-customize
   consult-ripgrep :keymap m-consult-ripgrep-map
   m-cr-same-extension :keymap m-consult-ripgrep-map)

  (defun m-cr-up-directory ()
    (interactive)
    (let ((parent-dir (file-name-directory (directory-file-name default-directory))))
      (when parent-dir
        (run-at-time 0 nil
                     #'consult-ripgrep
                     parent-dir
                     (ignore-errors
                       (buffer-substring-no-properties
                        (1+ (minibuffer-prompt-end)) (point-max))))))
    (minibuffer-quit-recursive-edit))

  (defun m--consult-ripgrep-filter (glob)
    (save-excursion
      (move-end-of-line 1)
      (unless (string-match-p " --" (minibuffer-contents))
        (insert " --"))
      (insert (format " -g \"%s\"" glob))))

  ;; HACK: this is a hacky solution to get the file name of the current candidate, parsing as string
  (defun m--consult-get-current-candidate-file ()
    (let ((str (vertico--candidate)))
      (car (string-split str ":"))))

  (defun m-cr-include-extension ()
    (interactive)
    (m--consult-ripgrep-filter
     (concat "*."
             (file-name-extension (m--consult-get-current-candidate-file)))))

  (defun m-cr-exclude-extension ()
    (interactive)
    (m--consult-ripgrep-filter
     (concat "!*."
             (file-name-extension (m--consult-get-current-candidate-file)))))

  (defun m-cr-exclude-directory ()
    (interactive)
    (m--consult-ripgrep-filter
     (concat "!"
             (file-name-directory (m--consult-get-current-candidate-file)))))

  ;; bind keys
  (general-def m-consult-ripgrep-map
    "C-c i" #'m-cr-include-extension
    "C-c e" #'m-cr-exclude-extension
    "C-c d" #'m-cr-exclude-directory
    "C-c u" #'m-cr-up-directory))

(defconst m-corfu-auto t) ;; whether to use corfu-auto or candidate overlay

(use-package corfu
  :if t
  :defer 1
  :commands (global-corfu-mode)
  ;; Enable Corfu only for certain modes. See also `global-corfu-modes'.
  ;; :hook ((prog-mode . corfu-mode)
  ;;        (shell-mode . corfu-mode)
  ;;        (eshell-mode . corfu-mode))

  ;; Recommended: Enable Corfu globally.  This is recommended since Dabbrev can
  ;; be used globally (M-/).  See also the customization variable
  ;; `global-corfu-modes' to exclude certain modes.
  :init
  (global-corfu-mode)
  :config
  (setopt
   corfu-cycle t                     ;; Enable cycling for `corfu-next/previous'
   corfu-auto m-corfu-auto           ;; Enable auto completion
   corfu-auto-prefix 3               ;; number of chars needed before auto is triggered
   corfu-auto-delay 0.1              ;; delay for auto completion
   corfu-separator ?\s               ;; Orderless field separator
   corfu-quit-at-boundary 'separator ;; Quit at boundary, unless separator inserted
   corfu-quit-no-match 'separator    ;; Quit if no match, unless separator inserted
   corfu-preview-current t           ;; Enable current candidate preview
   corfu-preselect 'valid            ;; Preselect prompt if valid, otherwise first candidate
   corfu-on-exact-match 'insert      ;; Insert candidate and quit
   corfu-scroll-margin 1             ;; Use scroll margin
   ))

;; A few more useful configurations...
(use-package emacs
  :if t
  :demand t
  :custom
  ;; TAB cycle if there are only few candidates
  ;; (completion-cycle-threshold 3)

  ;; Enable indentation+completion using the TAB key.
  ;; `completion-at-point' is often bound to M-TAB.
  (tab-always-indent 'complete))

(use-package corfu-terminal
  :unless (display-graphic-p)
  :demand t
  :after corfu
  :commands (corfu-terminal-mode)
  :config
  (corfu-terminal-mode +1))

(use-package corfu
  :if t
  :defer 1
  :commands (corfu-next
             corfu-previous
             corfu-scroll-up
             corfu-scroll-down
             corfu-quit
             corfu-insert-separator)
  :config
  ;; See https://github.com/minad/corfu/issues/12#issuecomment-868926083
  ;; for the workaround I'm using
  (if m-corfu-auto
      (progn
        (general-def
          :keymaps 'completion-in-region-mode
          :definer 'minor-mode
          :states 'insert
          :predicate 'corfu-mode
          "C-n" #'corfu-next
          "C-p" #'corfu-previous
          "C-f" #'corfu-scroll-up ; C-f, C-b are evil-like
          "C-b" #'corfu-scroll-down
          "C-c SPC" #'corfu-insert-separator ; M-SPC does same thing
          "<escape>" (defun m-corfu-quit ()
                       (interactive)
                       (call-interactively #'corfu-quit)
                       (if (fboundp 'evil-normal-state)
                           (call-interactively #'evil-normal-state))))
        (general-unbind corfu-map
          "RET" ; unbind ret, because i want newlines
          ))
    (general-def
      :keymaps 'completion-in-region-mode
      :definer 'minor-mode
      :states 'insert
      :predicate 'corfu-mode
      "C-n" #'corfu-next
      "<tab>" #'corfu-next
      "C-p" #'corfu-previous
      "<backtab>" #'corfu-previous
      "C-f" #'corfu-scroll-up ; C-f, C-b are evil-like
      "C-b" #'corfu-scroll-down
      "SPC" #'corfu-insert-separator
      "<escape>" (defun m-corfu-quit ()
                   (interactive)
                   (call-interactively #'corfu-quit)
                   (call-interactively #'evil-normal-state)))))

;; show documentation in echo area
(use-package corfu-echo
  :disabled ; disabled in favor of corfu-popupinfo
  :init
  (setopt corfu-echo-delay '(1.0 0.5)) ; time until docs show in echo area
  (corfu-echo-mode 1))

;; track items inserted with corfu and sort by that
(use-package corfu-history
  :if t
  :demand t
  :after (corfu)
  :commands (corfu-history-mode)
  :config
  (corfu-history-mode 1))

;; provide bindings to look up docs for things in corfu
(use-package corfu-info
  :unless (display-graphic-p)
  :demand t
  :after (corfu)
  :commands (corfu-info-location
             corfu-info-documentation)
  :config
  (general-def
    :keymaps 'completion-in-region-mode
    :definer 'minor-mode
    :states 'insert
    :predicate 'corfu-mode
    "C-l" #'corfu-info-location ; shows source (in separate buffer)
    "C-d" #'corfu-info-documentation ; shows help (in separate buffer)
    ))

;; this doesn't work in terminal, see https://github.com/minad/corfu/issues/248
;; so we just use corfu-info in terminal instead
(use-package corfu-popupinfo
  :when (display-graphic-p)
  :demand t
  :after (corfu)
  :commands (corfu-popupinfo-mode
             corfu-popupinfo-location
             corfu-popupinfo-documentation
             corfu-popupinfo-scroll-down
             corfu-popupinfo-scroll-up)
  :config
  (corfu-popupinfo-mode 1)
  (setopt corfu-popupinfo-delay '(1.0 . 0.8))
  (general-def
    :keymaps 'completion-in-region-mode
    :definer 'minor-mode
    :states 'insert
    :predicate 'corfu-mode
    "C-l" #'corfu-popupinfo-location ; shows source
    "C-d" #'corfu-popupinfo-documentation ; shows help
    "C-y" #'corfu-popupinfo-scroll-down
    "C-e" #'corfu-popupinfo-scroll-up))

(use-package corfu-candidate-overlay
  :unless m-corfu-auto
  :demand t
  :after (corfu)
  :commands (corfu-candidate-overlay-mode
             corfu-candidate-overlay-at-word-boundary-p
             corfu-candidate-overlay--show
             corfu-candidate-overlay--get-overlay-property
             corfu-candidate-overlay-complete-at-point)
  :config
  (corfu-candidate-overlay-mode +1)
  ;; in insert mode C-f will either insert the completion or do forward char if completion not shown.
  (defun m-complete-or-forward-char ()
    (interactive)
    ;; condition taken from implementation of #'corfu-candidate-overlay-complete-at-point
    (if (and corfu-candidate-overlay-mode
             corfu-candidate-overlay--overlay
             (overlayp corfu-candidate-overlay--overlay)
             (corfu-candidate-overlay-at-word-boundary-p)
             (progn
               (corfu-candidate-overlay--show)
               (not
                (string=
                 (corfu-candidate-overlay--get-overlay-property 'after-string)
                 ""))))
        (call-interactively #'corfu-candidate-overlay-complete-at-point)
      (call-interactively #'forward-char)))
  (general-def 'insert
    "C-f" #'m-complete-or-forward-char
    "<right>" #'m-complete-or-forward-char))

(use-package corfu
  :if t
  :defer 1
  :config
  (add-hook 'eshell-mode-hook (defun m-disable-corfu ()
                                (corfu-mode -1))))

(use-package corfu
  :if t
  :demand t
  :commands (corfu-mode)
  :after (evil)
  :config
  ;; HACK disable corfu in evil-ex.
  (setopt global-corfu-minibuffer
          (defun m-disable-in-evil-ex ()
            (and (not (eq this-command 'evil-ex))
                 (local-variable-p 'completion-at-point-functions)))))

(use-package corfu
  :if t
  :defer 3
  :commands (global-corfu-mode)
  :config
  (add-hook 'm-macro-enter-hook
            (defun m-disable-corfu-globally ()
              (global-corfu-mode -1)))
  (add-hook 'm-macro-exit-hook
            (defun m-enable-corfu-globally ()
              (global-corfu-mode +1))))

;; integrate embark and consult
;; (use-package embark-consult)

(use-package yasnippet
  :if t
  :defer 1
  :init
  ;; where yas looks for snippets defaults to ~/.emacs.d/snippets this won't work
  ;; with my guix home set up so point it to the actual directory instead
  (setopt yas-snippet-dirs `(,(expand-file-name "snippets" "~/sfs/dotfiles/emacs")))
  ;; we don't want final newlines in snippet files
  (add-hook 'snippet-mode-hook
            (defun m-disable-final-newline ()
              (setq-local require-final-newline nil)))
  :config
  (yas-global-mode 1))

(use-package org
  :if t
  :defer 3
  :commands (org-meta-return)
  :preface
  (declare-function org-in-item-p "org-list")
  (declare-function evil-open-below "evil")
  (declare-function evil-open-above "evil")
  (declare-function evil-append "evil")
  :init
  (add-hook 'org-mode-hook
            (defun m-org-setup ()
              (visual-line-mode 1)))
  (setopt
   ;; where to look for org files
   org-directory (expand-file-name "org" "~/sfs")
   ;; highlight latex
   org-highlight-latex-and-related '(native latex entities) ; script can be part of this list, but changes things following _
   ;; use ATTR for image width or don't resize otherwise
   org-image-actual-width nil
   ;; TODO customize these 2
   ;; org-capture-templates
   ;; org-capture-templates-contexts
   ;; non-nil means use date at point when capturing from agendas
   ;; org-capture-use-agenda-date t
   ;; where to put captures if can't find anywhere else
   org-default-notes-file (expand-file-name "notes/default.org" org-directory)
   ;; start with org-indent-mode on
   org-startup-indented t
   ;; don't indent subtrees. this means org indent mode just makes lists better
   org-indent-indentation-per-level 0
   ;; remove CLOSED: time-stamp when switching to non-todo state
   org-closed-keep-when-no-todo t
   ;; non-nil means show full links by default. toggle with `org-toggle-link-display'
   org-link-descriptive nil
   ;; log a timestamp when refiling
   org-log-refile 'time
   ;; TODO configure org refile
   ;; org-refile-targets
   ;; if org-mode called in an empty file, insert file local var for it to be org-mode
   org-insert-mode-line-in-empty-file t
   ;; show inline images by default
   org-startup-with-inline-images t
   ;; show latex preview by default
   org-startup-with-latex-preview t
   ;; M-RET will not split lines
   org-M-RET-may-split-line nil
   ;; TODO (customize-group 'org-archive)
   ;; tab should only cycle at start of headlines
   org-cycle-emulate-tab 'exc-hl-bol
   ;; how much extra indentation to add to src blocks
   org-edit-src-content-indentation 0
   ;; after 3 seconds idle tame auto save src blocks to org buffer
   org-edit-src-auto-save-idle-delay 3
   ;; preserve leading whitespace characters on export
   org-src-preserve-indentation t
   ;; use display-buffer to determine how to display src buffers
   org-src-window-setup 'plain
   ;; default is 2. in my case i think this is fine for performance
   org-imenu-depth 6))

(use-package org
  :if t
  :demand t
  :after (evil)
  :commands (org-meta-return
             org-metaleft
             org-metadown
             org-metaup
             org-metaright
             org-shiftleft
             org-shiftdown
             org-shiftup
             org-shiftright
             org-todo
             org-priority
             org-edit-special
             org-export-dispatch)
  :preface
  (declare-function org-in-item-p "org-list")
  (declare-function evil-open-below "evil")
  (declare-function evil-open-above "evil")
  (declare-function evil-append "evil")
  (declare-function evil-insert "evil")
  :config
  ;; make custom defs for o and O which are context aware
  (general-def 'normal org-mode-map
    "o" (defun m-org-evil-open-below ()
          (interactive)
          (if (and (org-in-item-p) (not (looking-at-p "^$")))
              (progn
                (move-end-of-line 1)
                (call-interactively #'org-meta-return)
                (if (looking-at-p " ::")
                    (call-interactively #'evil-insert)
                  (call-interactively #'evil-append)))
            (call-interactively #'evil-open-below))))

  (general-def 'normal org-mode-map
    "O" (defun m-org-evil-open-above ()
          (interactive)
          (if (and (org-in-item-p) (not (looking-at-p "^$")))
              (progn
                (move-beginning-of-line 1)
                (call-interactively #'org-meta-return)
                (if (looking-at-p " ::")
                    (call-interactively #'evil-insert)
                  (call-interactively #'evil-append)))
            (call-interactively #'evil-open-above))))

  (m-mode-def 'normal org-mode-map
    "b" #'org-babel-tangle
    "l" #'org-toggle-link-display
    "h" #'org-metaleft
    "j" #'org-metadown
    "k" #'org-metaup
    "l" #'org-metaright
    "H" #'org-shiftleft
    "J" #'org-shiftdown
    "K" #'org-shiftup
    "L" #'org-shiftright
    "t" #'org-todo
    ;; TODO bind keys related to agenda (like schedule), archive, refile
    "p" #'org-priority
    "e" #'org-edit-special
    "E" #'org-export-dispatch))

(use-package org
  :if t
  :demand t
  :config
  (define-advice org-edit-src-exit (:before (&rest _) format-buffer)
    (let ((inhibit-message t))
      (indent-region (point-min) (point-max)))))

;; TODO: integrate consult-org-agenda

(use-package org-agenda
  :after (org)
  :demand t
  :config
  (setopt org-agenda-files (if (not m-ww?) ; can be relative to `org-directory'
                               '("/home/popska/sfs/org/agenda/")
                             '("c:/ddev/notes/org/agenda.org"))
          org-agenda-start-with-log-mode '(closed clock) ; track when tasks get done see `org-agenda-log-mode-items'
          org-log-done 'time ; add time stamp when commands are finished
          org-log-into-drawer t ; makes state change notes & time stamps go in drawer for cleaner look
          org-todo-keywords (if (not (or m-ww? m-wl?))
                                '((sequence "TODO(t)" "IDEA(i)" "NEXT(n!)" "HOLD(h@/!)" "WIP(w@)" "|" "DONE(d)" "CANCELED(c@)"))
                              '((sequence "TODO(t)" "IDEA(i)" "NEXT(n!)" "HOLD(h@/!)" "WIP(w@)" "PR(p!)" "|" "DONE(d)" "CANCELED(c@)")))
          org-agenda-span 'week ; show 1 week be default
          org-agenda-start-on-weekday nil ; dont care about weekly
          org-agenda-dim-blocked-tasks nil ; don't dim tasks even if there are unfinished subtasks
          org-agenda-skip-scheduled-if-done nil ; whether to show tasks done ahead of schedule

          ;; define commands for custom agenda views
          org-agenda-custom-commands
          '(("v" "Verbose"
             ((agenda "" ((org-deadline-warning-days 7)))))

            ("b" "Brief"
             ((agenda "" ((org-deadline-warning-days 3)
                          (org-agenda-span 'day)
                          (org-agenda-start-day "0d")))
              (todo "NEXT"
                    ((org-agenda-overriding-header "Next Tasks")))))

            ("n" "Next Tasks"
             ((todo "NEXT"
                    ((org-agenda-overriding-header "Next Tasks")))))

            ("w" "Workflow Status"
             ((todo "IDEA"
                    ((org-agenda-overriding-header "Ideas I've Had")
                     (org-agenda-files org-agenda-files)))
              (todo "TODO"
                    ((org-agenda-overriding-header "Things To Do")
                     (org-agenda-files org-agenda-files)))
              (todo "WIP"
                    ((org-agenda-overriding-header "Look At Later")
                     (org-agenda-files org-agenda-files)))
              (todo "DONE"
                    ((org-agenda-overriding-header "Completed Tasks")
                     (org-agenda-files org-agenda-files)))
              (todo "CANCELED"
                    ((org-agenda-overriding-header "Canceled Projects")
                     (org-agenda-files org-agenda-files))))))

          ;; define a place to archive done tasks to get em out of view
          org-refile-targets '(("archive.org" :maxlevel . 1))

          ;; faces
          org-priority-faces ; make priorities distinguishable
          '((?A . '(bold org-priority))
            (?B . org-priority)
            (?C . '(shadow org-priority)))

          ;; per org-todo-keyword-faces documentation by setting each face
          ;; to a color string it inherits rest of face from org-todo face
          org-todo-keyword-faces
          '(("TODO" . org-todo) ; default
            ("IDEA" . "#6ae4b9")
            ("NEXT" . "#c6daff") ; taken from hl-todo-keyword-faces
            ("HOLD" . "#d0bf8f") ; taken from hl-todo-keyword-faces
            ("WIP" . "#fec43f")
            ("PR" . "#6ae4b9")
            ("DONE" . "#afd8af")
            ("CANCELED" . "#aea4af")))
  (defvar m-agenda-update-timer
    (run-with-timer 300 300 (defun m-agenda-update ()
                              (when (time-less-p (seconds-to-time 10) (current-idle-time))
                                (org-agenda-maybe-redo)))))
  ;; add advicew to save buffers
  (advice-add 'org-agenda-todo :after
              (defun m-agenda-after-todo (&rest _)
                (run-with-timer 10 nil #'org-save-all-org-buffers))))




;; automatically refile all done entries in the current buffer
;; (defun m-agenda-refile-buffer ()
;;   (interactive)
;;   (save-excursion
;;     (goto-char (point-min))
;;     (let ((term (eval `(rx (: bol (+ "*") (+ " ")
;;                               ,(cons 'or org-done-keywords-for-agenda))))))
;;       (while (re-search-forward term nil t)
;;         (org-refile nil "archive.org" '("Dailies" "archive.org" nil 0))))))

;; TODO: bind m-agenda-refile-buffer
;;(map!
;; :map org-mode-map
;; :localleader
;; :desc "Refile all in buffer to archive.org" "ra" #'m-agenda-refile-buffer)

;; habits setup

(use-package org-habit
  :after org-agenda
  :init
  (add-to-list 'org-modules 'org-habit)
  (setq org-habit-graph-column 60))

;; TODO: add org capture templates

(use-package project
  :if t
  :defer 1
  :init
  (general-unbind m-project-map
    "g" ; project-find-regexp (using consult commands instead)
    "v" ; project-vc-dir (using magit)
    "C-b" ; project-list-buffers
    "G" ; project-or-external-find-regexp
    "F" ; project-or-external-find-file
    )
  :config
  (setopt project-vc-include-untracked t ; include files not yet tracked by git when searching
          project-kill-buffers-display-buffer-list t ; show buffers killed when killing all project buffers
          ))

(use-package dired
  :if t
  :defer 1
  :preface
  (declare-function dired-directory-changed-p "dired")
  (declare-function dired-buffer-stale-p "dired")
  :config
  (setopt
   ;; if two direds open copy will automatically suggest copying to the other one
   dired-dwim-target t
   ;; auto revert dired buffers
   ;; dired-auto-revert-buffer #'dired-directory-changed-p
   dired-auto-revert-buffer #'dired-buffer-stale-p
   ;; switches passed to ls
   ;; -A all but . and ..
   ;; -l (needed)
   ;; -h human readable sizes
   ;; --group-directories-first
   ;; -t sort by modification time (newest first)
   ;; -X sort alphabetically by extension
   dired-listing-switches "-Alht --group-directories-first"
   ;; kill old buffer when opening new dired buffer to prevent buffer clutter
   ;; (there can still be multiple direds, but i have to invoke dired multiple times)
   dired-kill-when-opening-new-dired-buffer t
   ;; make destination directories for copies/renames if no exist
   dired-create-destination-dirs 'always
   ;; if copying to "test/" and the dir doesn't exist create the dir
   dired-create-destination-dirs-on-trailing-dirsep t
   ;; preserve the modification time of old file "cp -p"
   dired-copy-preserve-time t
   ;; recursively copy? set to 'top for confirmation prompt
   dired-recursive-copies t
   ;; allow recursive deletes. since i'm using trash, not as worried!
   dired-recursive-deletes t
   )

  ;; dired-auto-revert-buffer doesn't seem as aggressive as I want when it comes to reverting buffers.
  ;; lets automatically revert whenever I change to a dired buffer
  (add-hook 'window-selection-change-functions
            (defun m-dired-revert-window-selection (&rest _)
              (when (eq major-mode 'dired-mode)
                (call-interactively #'revert-buffer-quick))))

  ;; disable evil matchit in dired because dired uses % a lot
  (add-hook 'dired-mode-hook
            (defun m-disable-evil-matchit ()
              (if (fboundp 'evil-matchit-mode)
                  (evil-matchit-mode -1)))))

(use-package wdired
  :if t
  :defer 1
  :config
  (setopt
   ;; allow changing file permissions in wdired
   wdired-allow-to-change-permissions t))

(use-package dired
  :if t
  :demand t
  :after (consult)
  :preface
  (declare-function consult--buffer-state "consult")
  (declare-function consult--buffer-query "consult")
  (declare-function consult--buffer-pair "consult")
  (declare-function consult-buffer "consult")
  :config
  (defvar consult-buffer-sources) ; silence warnings
  (defvar consult--source-buffer) ; silence warnings

  ;; add key to go straight to dired
  (defvar m-dired-source
    (list :name "Dired"
          :category 'buffer
          :face 'consult-buffer
          :history 'buffer-name-history
          :state #'consult--buffer-state
          :hidden nil
          :items
          (lambda ()
            (consult--buffer-query :mode 'dired-mode :as #'consult--buffer-pair))))
  (m-buffer-def '(normal visual emacs)
    "d" (defun m-consult-dired ()
          (interactive)
          (consult-buffer (list m-dired-source)))
    "D" #'dired)

  ;; add ability to narrow to dired
  (defvar m-dired-source-hidden
    `(:hidden t :narrow (?d . "Dired") ,@m-dired-source))
  (add-to-list 'consult-buffer-sources 'm-dired-source-hidden 'append)

  ;; make dired buffers not show up by default
  (add-hook 'm-consult-buffer-predicates
            (defun m-dired-pred (buffer)
              (let ((mm (buffer-local-value 'major-mode buffer)))
                (not (eq mm 'dired-mode))))))

(use-package dired
  :if t
  :demand t
  :after (evil)
  :commands (dired-up-directory
             dired-find-file
             dired-toggle-read-only
             dired-maybe-insert-subdir
             dired-kill-subdir
             dired-hide-subdir
             dired-next-subdir
             dired-prev-subdir
             dired-goto-subdir
             dired-goto-file
             dired-unmark-backward
             dired-mark-files-containing-regexp)
  :config
  (general-def 'normal dired-mode-map
    "h" #'dired-up-directory
    "l" #'dired-find-file
    "i" #'dired-toggle-read-only ; wdired
    ;; navigate using completion
    "f" (defun m-dired-goto-file ()
          (interactive)
          (call-interactively #'dired-goto-file)
          (call-interactively #'dired-find-file))
    "DEL" #'dired-unmark-backward ; u is the forward equivalent
    ;; also use "g j" and "g k" to navigate among directories
    "s i" #'dired-maybe-insert-subdir ; default in is "I"
    "s k" #'dired-kill-subdir
    "s h" #'dired-hide-subdir
    "s n" #'dired-next-subdir
    "s p" #'dired-prev-subdir
    "s g" #'dired-goto-subdir

    ;; also bound to % g
    "* g" #'dired-mark-files-containing-regexp))

(use-package nerd-icons-dired
  :if (not m-ww?)
  :demand t
  :commands(nerd-icons-dired-mode)
  :after (nerd-icons)
  :config
  (when (or m-wl? (file-exists-p (concat (getenv "HOME") "/.local/share/fonts/NFM.ttf")))
    (add-hook 'dired-mode-hook #'nerd-icons-dired-mode)))

(use-package diredfl
  :if t
  :defer 2
  :commands (diredfl-global-mode)
  :config
  (diredfl-global-mode +1))

(use-package emacs
  :if t
  :demand t
  :config
  (setopt delete-by-moving-to-trash t))

;;; evil mode setup
(use-package evil
  :if t
  :demand t
  :commands (evil-mode
             evil-next-visual-line
             evil-previous-visual-line
             evil-emacs-state
             evil-emacs-state-p
             evil-normal-state
             evil-normal-state-p
             evil-write-all
             evil-write
             evil-undo)
  :init
  (setopt evil-want-keybinding nil) ; required to be nil for evil-collection
  :config
  (setopt
   ;; keybindings
   evil-toggle-key "C-z" ; key that switches to emacs-state
   evil-want-C-i-jump t ; C-i has vim-like behavior

   ;; this means we'll want some other way of doing prefix args
   evil-want-C-u-delete t ; C-u deletes to indent in insert mode
   evil-want-C-u-scroll t ; C-u scrolls in normal mode

   evil-want-C-d-scroll t ; normal mode
   evil-want-C-w-delete t ; in insert mode

   evil-want-Y-yank-to-eol t ; Y is consistent with C and D

   evil-disable-insert-state-bindings nil ; keep insert key bindings vim-like


   ;; searching
   evil-search-module 'evil-search ; use Evil search so i get evil regexp
   evil-ex-search-vim-style-regexp t
   evil-regexp-search t ; use regular expressions by default
   evil-search-wrap t ; Whether to wrap the search. This should be nil
					; when doing macros

   ;; indentation
   evil-auto-indent t ; o and O indent
   evil-shift-width (if (or m-wl? m-ww?) 2 4) ; i use 2 exclusively at work

   ;; cursor movement
   evil-repeat-move-cursor t ; if . can move cursor
   evil-move-cursor-back t ; behavior when exiting insert state
   evil-move-beyond-eol nil ; cant move past eol, like vim
   evil-cross-lines nil ; h, l, t, T, f, F, etc. are only on one line. Should be nil for macros.
   evil-respect-visual-line-mode t ; navigate by visual lines, when visual-line-mode is on
   evil-track-eol t ; after $, j and k will navigate to eol
   evil-start-of-line t ; gg will go to bol, even if point isn't at bol initially

   ;; windows
   evil-auto-balance-windows t ; balance upon deletion/creation
   evil-vsplit-window-right t ; vsplit windows go on right
   evil-split-window-below t ; h split windows go below

   ;; misc
   evil-want-fine-undo t ; undo is done w/ emacs heuristics, so insert
					; mode edits can be more than 1 undo step
   evil-undo-system 'undo-redo ; required for C-r to work. Other options
					; are undo-tree or undo-fu if those
					; packages are installed, but undo-redo
					; is built into emacs 28+.
   evil-backspace-join-lines t ; backspace can delete \n
   evil-kbd-macro-suppress-motion-error nil ; ring bell when l and h hit
					; bol or eol
   evil-kill-on-visual-paste t ; place replaced text in kill ring upon
					; visual paste
   evil-complete-all-buffers t ; completion (C-n/p) looks at all buffers
   evil-want-empty-ex-last-command t ; : command has previous command
					; filled already
   evil-want-integration t ; integrates evil with other packages, such as avy
   )

  (m-leader 'normal
    "u" (defun m-double-universal-argument ()
          (interactive)
          (if current-prefix-arg
              (call-interactively #'universal-argument-more)
            (call-interactively #'universal-argument))))

  ;; make a binding to toggle between evil and emacs
  (m-toggle-def '(normal emacs)
    "e" (defun m-toggle-evil ()
          (interactive)
          (if (evil-normal-state-p)
              (call-interactively #'evil-emacs-state)
            (call-interactively #'evil-normal-state))))

  ;; XXX: this screws with M- keys in WSL. See what doom does maybe?
  ;; (unless (eq m-system 'work)
  ;;   (global-set-key [escape] #'keyboard-escape-quit))
  (global-set-key [escape] #'keyboard-escape-quit)

  (general-def 'normal
    "j" #'evil-next-visual-line
    "k" #'evil-previous-visual-line)

  (general-def 'normal
    "ZA" #'evil-write-all
    "ZW" #'evil-write)

  ;; make SPC w does evil window commands
  (m-leader 'normal
    "w" (cons "window" evil-window-map))

  ;; In visual mode, "u" should be undo instead of evil-downcase. If want to downcase press "g u"
  (general-def 'visual
    "u" #'evil-undo)

  ;; and turn it on!
  (evil-mode 1))

(use-package evil
  :if t
  :demand t
  :config
  (require 'seq)
  (seq-do (lambda (mode)
            (add-to-list 'evil-emacs-state-modes mode)
            (delq mode evil-motion-state-modes))
	  '(;;helpful-mode
	    ;;dired-mode ;; using evil collection
	    ;;magit-mode ;; using evil collection
	    ;;Info-mode
      )))

(use-package evil
  :if t
  :demand t
  :commands (evil-range)
  :config
  (evil-define-text-object m-buffer-both (_count &optional _beg _end type)
    "selects entire buffer"
    (evil-range (point-min) (point-max) type))

  (evil-define-text-object m-defun-both (_count &optional _beg _end type)
    "selects the defun"
    (let ((bounds (bounds-of-thing-at-point 'defun)))
      (evil-range (car bounds) (cdr bounds) type)))

  (evil-define-text-object m-url-or-path-both (_count &optional _beg _end type)
    "selects a url or path depending on what is at point"
    (let ((bounds (bounds-of-thing-at-point 'url)))
      (unless bounds
	(setq bounds (bounds-of-thing-at-point 'filename)))
      (evil-range (car bounds) (- (cdr bounds) (if (evil-visual-state-p) 1 0)) type)))

  (evil-define-text-object m-sexp-both (_count &optional _beg _end type)
    "selects the sexp at point"
    (let ((bounds (bounds-of-thing-at-point 'sexp)))
      (evil-range (car bounds) (- (cdr bounds) (if (evil-visual-state-p) 1 0)) type)))

  ;; define all keys
  (general-def '(inner outer) ; see general-keymap-aliases
    "f" #'m-buffer-both
    "d" #'m-defun-both
    "u" #'m-url-or-path-both
    "x" #'m-sexp-both))

(use-package evil-matchit
  :if t
  :demand t
  :after evil
  :commands (global-evil-matchit-mode)
  :config
  (global-evil-matchit-mode 1))

(use-package evil-nerd-commenter
  :if t
  :demand t
  :after (evil)
  :commands (evilnc-comment-operator
             evilnc-copy-and-comment-operator
             evilnc-yank-and-comment-operator)
  :init
  (general-def '(normal visual)
    "gc" #'evilnc-comment-operator
    "gC" #'evilnc-copy-and-comment-operator ; also see evilnc-yank-and-comment-operator
    )
  ;; The text objects don't seem to work that well
  ;; (general-def 'inner
  ;;   "c" #'evilnc-inner-comment)
  ;; (general-def 'outer
  ;;   "c" #'evilnc-outer-commenter)
  )

(use-package evil
  :if t
  :demand t
  :config
  ;; we want evil-search to ring bell if end of buffer
  (m-set-kmacro evil-search-wrap nil)
  ;; we want f, t to be restricted to lines
  (m-set-kmacro evil-cross-lines nil))

(use-package evil-collection
  :if t
  :demand t
  :after (evil)
  :commands 'evil-collection-init
  :init
  (evil-collection-init))

;; https://emacs.stackexchange.com/questions/31454/evil-mode-how-to-run-evil-indent-on-the-text-ive-just-pasted
(use-package evil
  :if t
  :demand t
  :config
  (defun m-get-evil-paste-begin ()
    (max (evil-get-marker ?\[) (point-min)))

  (defun m-get-evil-paste-end ()
    (min (evil-get-marker ?\]) (point-max)))

  (defun m-paste-and-indent-after ()
    (interactive)
    (with-undo-amalgamate
      (call-interactively #'evil-paste-after)
      (save-excursion (indent-region (m-get-evil-paste-begin) (m-get-evil-paste-end)))))

  (defun m-paste-and-indent-before ()
    (interactive)
    (with-undo-amalgamate
      (call-interactively #'evil-paste-before)
      (save-excursion (indent-region (m-get-evil-paste-begin) (m-get-evil-paste-end)))))

  (defun m-yank-pop-and-indent ()
    (interactive)
    (with-undo-amalgamate
      (when (memq last-command '(m-paste-and-indent-after m-paste-and-indent-before))
        (m-f "from %s to %s" (m-get-evil-paste-begin) (m-get-evil-paste-end))
        (delete-region (m-get-evil-paste-begin) (m-get-evil-paste-end)))
      (when (region-active-p)
        ;; this pushes region to the kill-ring before the yank, we'll call this a feature and not a bug.
        (call-interactively #'kill-region))
      (call-interactively #'consult-yank-pop)
      (save-excursion (indent-region (mark) (point)))))

  (general-def '(normal visual)
    "p" #'m-paste-and-indent-after
    "P" #'m-paste-and-indent-before
    "C-p" #'m-yank-pop-and-indent)

  ;; use the normal bindings in python and yaml, because in a language like that trying to be smart about indent is bad idea
  (general-def '(normal visual) '(python-mode-map python-ts-mode-map yaml-ts-mode-map)
    "p" #'evil-paste-after
    "P" #'evil-paste-before
    "C-p" #'evil-paste-pop))

(use-package evil
  :if t
  :demand t
  :preface
  (declare-function replace-region-contents "subr-x")
  (declare-function evil-ex-define-cmd "evil-ex")
  :config
  (require 'subr-x)
  (evil-define-operator m-evil-swap (beg end string0 string1 &optional _flags)
    ;; live highlighting this would be cool, but is insanely complicated!
    ;; also would be nice to use flags
    :repeat nil
    :jump t
    :move-point nil
    :motion evil-line
    (interactive "<r></>")
    (with-undo-amalgamate
      (replace-region-contents
       beg end
       (lambda ()
         (let ((random-string (format "xxx%dxxx" (random))))
           (goto-char (point-min))
           (while (search-forward string0 nil t)
             (replace-match random-string))
           (goto-char (point-min))
           (while (search-forward string1 nil t)
             (replace-match string0))
           (goto-char (point-min))
           (while (search-forward random-string nil t)
             (replace-match string1)))
         (buffer-string)))))

  (evil-ex-define-cmd "sw[ap]" 'm-evil-swap))

(use-package evil-goggles
  :if t
  :defer 3
  :commands (evil-goggles-mode
             evil-goggles-use-diff-faces)
  :init
  (setopt
   ;; whether to pulse the hint or just appear/disappear
   evil-goggles-pulse 'display-graphic-p
   ;; default is 0.2s which i think is a bit too long
   evil-goggles-duration 0.1

   ;; Default for both these is nil (use evil-goggles-duration). I
   ;; want the blocking duration to be short, but I don't mind async
   ;; duration longer. If these are set then evil-goggles-duration is
   ;; unused. Async duration applies to commands like =, where the
   ;; command is executed immediately and the hint is shown async.
   ;; Pressing any key causes the hint to go away. On the other hand
   ;; blocking applies to commands like d, where the command isn't
   ;; executed until after the hint is shown.
   evil-goggles-blocking-duration 0.08
   evil-goggles-async-duration 0.3)
  :config
  (evil-goggles-mode)

  ;; optionally use diff-mode's faces; as a result, deleted text
  ;; will be highlighed with `diff-removed` face which is typically
  ;; some red color (as defined by the color theme)
  ;; other faces such as `diff-added` will be used for other actions
  (evil-goggles-use-diff-faces))

(use-package evil-numbers
  :if t
  :demand t
  :commands (evil-numbers/inc-at-pt-incremental
             evil-numbers/dec-at-pt-incremental)
  :after (evil)
  :config
  (general-def '(normal visual)
    "g +" #'evil-numbers/inc-at-pt-incremental
    "g -" #'evil-numbers/dec-at-pt-incremental))

(use-package evil-surround
  :if t
  :demand t
  :after (evil)
  :commands (global-evil-surround-mode)
  :config
  (global-evil-surround-mode 1))

(use-package vterm
  :if (not m-ww?)
  :defer 2
  :init
  (setopt
   ;; %s is replaced with $TITLE provided by the shell
   ;; see https://github.com/akermu/emacs-libvterm?tab=readme-ov-file#vterm-buffer-name-string
   vterm-buffer-name-string "vterm@%s"
   ;; default is 1000
   vterm-max-scrollback 6000
   ;; use zsh
   vterm-shell "zsh"))

(use-package vterm
  :if t
  :demand t
  :after (consult)
  :commands (vterm)
  :preface
  (declare-function consult--buffer-state "consult")
  (declare-function consult--buffer-query "consult")
  (declare-function consult--buffer-pair "consult")
  (declare-function consult-buffer "consult")
  :config
  (defvar consult-buffer-sources) ; silence warnings
  (defvar consult--source-buffer) ; silence warnings

  ;; add key to go straight to vterm
  (defvar m-vterm-source
    (list :name "Vterm"
          :category 'buffer
          :face 'consult-buffer
          :history 'buffer-name-history
          :state #'consult--buffer-state
          :hidden nil
          :items
          (lambda ()
            (consult--buffer-query :mode 'vterm-mode :as #'consult--buffer-pair))))
  (m-buffer-def '(normal visual emacs)
    "v" (defun m-consult-vterm ()
          (interactive)
          (consult-buffer (list m-vterm-source)))
    "V" #'vterm)

  ;; add ability to narrow to vterm
  (defvar m-vterm-source-hidden
    `(:hidden t :narrow (?v . "vterm") ,@m-vterm-source))
  (add-to-list 'consult-buffer-sources 'm-vterm-source-hidden 'append)

  ;; make vterm buffers not show up by default
  (add-hook 'm-consult-buffer-predicates
            (defun m-vterm-pred (buffer)
              (let ((mm (buffer-local-value 'major-mode buffer)))
                (not (eq mm 'vterm-mode))))))

(use-package vterm
  :if (not m-ww?)
  :demand t
  :after (project)
  :commands (vterm)
  :preface
  (declare-function project-root "project")
  (declare-function project-prefixed-buffer-name "project")
  :config
  (defvar m-vterm-project-buffer nil
    "The current vterm project buffer.")
  (general-def m-project-map
    "v" (defun m-project-vterm ()
          (interactive)
          (if (buffer-live-p m-vterm-project-buffer)
              (pop-to-buffer m-vterm-project-buffer)
            (let ((default-directory (project-root (project-current t))))
              (call-interactively #'vterm)
              (setq m-vterm-project-buffer (current-buffer)))))))

(use-package vterm
  :if (not m-ww?)
  :defer 2
  :commands (vterm-send-key
             vterm-send-next-key)
  :config
  ;; set keys emacs interprets and aren't sent to vterm
  ;; default list ("C-c" "C-x" "C-u" "C-g" "C-h" "C-l" "M-x" "M-o" "C-y" "M-y")
  (add-to-list 'vterm-keymap-exceptions "C-M-o" t #'string=)
  (add-to-list 'vterm-keymap-exceptions "M-:" t #'string=)
  (add-to-list 'vterm-keymap-exceptions "SPC" t #'string=)
  (add-to-list 'vterm-keymap-exceptions "C-q" t #'string=)

  ;; HACK this solution involves zsh-vi-mode setting which mode it's in on a hook.
  ;; I think a simpler solution would be if I could figure out how to read $ZVM_MODE
  ;; in the running vterm instance
  (defun m-zsh-mode-set-normal ()
    (setq-local m-vterm-zsh-mode 'normal))
  (defun m-zsh-mode-set-insert ()
    (setq-local m-vterm-zsh-mode 'insert))
  (defun m-zsh-mode-set-visual ()
    (setq-local m-vterm-zsh-mode 'visual))
  (defun m-zsh-mode-set-visual-line ()
    (setq-local m-vterm-zsh-mode 'visual-line))
  (defun m-zsh-mode-set-replace ()
    (setq-local m-vterm-zsh-mode 'replace))

  (add-hook 'vterm-mode-hook
            #'m-zsh-mode-set-insert)

  (add-to-list 'vterm-eval-cmds '("m-zsh-mode-set-normal" m-zsh-mode-set-normal))
  (add-to-list 'vterm-eval-cmds '("m-zsh-mode-set-insert" m-zsh-mode-set-insert))
  (add-to-list 'vterm-eval-cmds '("m-zsh-mode-set-visual" m-zsh-mode-set-visual))
  (add-to-list 'vterm-eval-cmds '("m-zsh-mode-set-visual-line" m-zsh-mode-set-visual-line))
  (add-to-list 'vterm-eval-cmds '("m-zsh-mode-set-replace" m-zsh-mode-set-replace))

  (general-def 'emacs vterm-mode-map
    "SPC" (defun m-vterm-spc ()
            (interactive)
            (if (eq m-vterm-zsh-mode 'insert)
                (vterm-send-key " ")
              (setq unread-command-events
                    (mapcar (lambda (e) `(t . ,e))
                            (listify-key-sequence (kbd "C-c"))))))
    "C-q" #'vterm-send-next-key)

  ;; In TTY ESC is used for Meta. If I just bind "ESC", then none of my meta commands work.
  ;; Therefore we use this workaround so we can press "ESC ESC" to actually send an escape
  ;; character to the vterm process. Note in graphical sessions everything works how I want,
  ;; so there is no need for a workaround like this.
  (unless (display-graphic-p)
    (general-def 'emacs vterm-mode-map
      "ESC ESC" (defun m-vterm-esc ()
                  (interactive)
                  (vterm-send-key "")))))

(use-package vterm
  :if (not m-ww?)
  :demand t
  :after (evil)
  :preface
  (declare-function evil-normal-state "evil")
  (declare-function evil-emacs-state "evil")
  (declare-function evil-set-initial-state "evil")
  :config
  (general-def 'emacs vterm-mode-map
    ;; use standard yank-pop instead of consult because consult doesn't work
    "M-y" #'yank-pop)

  (evil-set-initial-state 'vterm-mode 'emacs)
  ;; Don't set cursor using evil in vterm mode (we want zsh to do it)
  (define-advice evil-set-cursor (:before-until (_specs) dont-do-it)
    (and (eq major-mode 'vterm-mode)
         (not vterm-copy-mode)))
  (add-hook 'vterm-copy-mode-hook
            (defun m-vterm-toggle-evil ()
              ;; hook run enter or leave copy mode so we want this to toggle
              (if vterm-copy-mode
                  (call-interactively #'evil-normal-state)
                (call-interactively #'evil-emacs-state)))))

(use-package vterm
  :if (not m-ww?)
  :defer 2
  :config
  (add-hook 'vterm-mode-hook
            (defun m-disable-hl-line-mode ()
              (setq-local global-hl-line-mode nil)
              (hl-line-mode -1)))
  (add-hook 'vterm-copy-mode-hook
            (defun m-enable-hl-line-mode ()
              ;; hook run enter or leave copy mode so we want this to toggle
              (call-interactively #'hl-line-mode))))

(use-package vterm
  :if (not m-ww?)
  :demand t
  :after (with-editor)
  :preface
  (declare-function with-editor-export-editor "with-editor")
  :config
  ;; depth 10, because we want this to run after disabling line numbers
  (add-hook 'vterm-mode-hook #'with-editor-export-editor 10))

(use-package magit
  :if (not m-ww?)
  :defer 3
  :diminish 'magit-wip-mode
  :commands (magit
             magit-dispatch
             magit-file-dispatch
             magit-wip-mode
             magit-display-buffer-same-window-except-diff-v1)
  :init
  (m-git-def 'normal
    "g" #'magit
    "d" #'magit-dispatch
    "f" #'magit-file-dispatch)
  :config
  (magit-wip-mode) ; try out wip mode, should make it harder to screw things up.
  ;; (evil-collection-require 'magit)
  ;; (evil-collection-magit-setup)
  (setopt magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)

  (add-hook 'git-commit-setup-hook
            (defun m-unlock-gpg-key ()
              ;; just some way to open the file forcing a pinentry
              (when (or m-eq? m-su?)
                (with-temp-buffer
                  (insert-file-contents (expand-file-name "~/sfs/dotfiles/misc/file.gpg")))))))

(use-package magit-todos
  :if (not m-ww?)
  :demand t
  :after (magit)
  :commands (magit-todos-mode)
  :config
  (magit-todos-mode +1))

(use-package magit-todos
  :if (not m-ww?)
  :demand t
  :after (magit)
  :config
  ;; set the regexp for what follows the todo keyword (i don't always have : at end)
  ;; default is the string minus the ? at the very end
  (setopt magit-todos-keyword-suffix "\\(?:[([][^])]+[])]\\)?:?"))

(use-package blamer
  :if (not m-ww?)
  :defer 3
  :commands (global-blamer-mode
             blamer-show-commit-info
             blamer-kill-ring-commit-hash)
  :init
  (setopt blamer-idle-time 3.0 ; default is much shorter than this, but i want non-invasive
          blamer-prettify-time-p t ; make times fancy
          blamer-min-offset 30 ; can be a bit closer to left
          ;; configure auto-blame on right
          blamer-author-formatter "  ✎ %s, "
          blamer-datetime-formatter "%s"
          blamer-commit-formatter " ● %s"
          blamer--overlay-popup-position 'smart ; try to be smart about where put popup
          )
  :config
  (global-blamer-mode +1)
  (m-git-def '(normal visual emacs)
    "b" (defun m-blame-current ()
          (interactive)
          (let ((tmp blamer-max-commit-message-length))
            (setq blamer-max-commit-message-length 60)
            (call-interactively #'blamer-show-commit-info)
            ;; blamer uses max-commit-length for both in buffer and popups, so we use this
            ;; hack to make it so the popups can show longer messages and it'll still work
            ;; with async
            (run-with-timer 1 nil (lambda ()
                                    (setq blamer-max-commit-message-length tmp)))))
    "y" #'blamer-kill-ring-commit-hash))

(use-package blamer
  :if (not m-ww?)
  :defer 3
  :commands (blamer--clear-overlay)
  :config
  (define-advice evil-next-visual-line (:before (&rest _) clear-blame)
    (when blamer--overlays
      (blamer--clear-overlay))))

(use-package diff-hl
  :if (not m-ww?)
  :defer 3
  ;; :diminish ?
  :commands (global-diff-hl-mode
             diff-hl-mode
             diff-hl-margin-mode
             diff-hl-next-hunk
             diff-hl-previous-hunk
             diff-hl-revert-hunk
             diff-hl-stage-current-hunk)
  :init
  ;; show on right to prevent clashes with evil-fringe-mode for example
  (setopt diff-hl-side 'right)
  :config
  (global-diff-hl-mode +1)
  (unless (display-graphic-p)
    ;; theres no fringe in console, so put it in margin
    (diff-hl-margin-mode))
  (m-git-def '(normal emacs)
    "n" #'diff-hl-next-hunk
    "p" #'diff-hl-previous-hunk
    "r" #'diff-hl-revert-hunk
    "s" #'diff-hl-stage-current-hunk)

  ;; need this to keep diff-hl up to date
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package diff-hl-dired
  :if (not m-ww?)
  :defer 3
  :commands (diff-hl-dired-mode-unless-remote)
  :config
  (add-hook 'dired-mode-hook #'diff-hl-dired-mode-unless-remote))

;; adapted from https://www.ovistoica.com/blog/2024-7-05-modern-emacs-typescript-web-tsx-config#orgb6fd011
(use-package treesit
  :unless m-ww?
  :defer t
  :mode (("\\.tsx\\'" . tsx-ts-mode)
         ("\\.js\\'"  . typescript-ts-mode)
         ("\\.mjs\\'" . typescript-ts-mode)
         ("\\.mts\\'" . typescript-ts-mode)
         ("\\.cjs\\'" . typescript-ts-mode)
         ("\\.ts\\'"  . typescript-ts-mode)
         ("\\.jsx\\'" . tsx-ts-mode)
         ("\\.json\\'" .  json-ts-mode)
         ("\\.Dockerfile\\'" . dockerfile-ts-mode)
         ("\\.sh\\'" . bash-ts-mode)
         ("\\.css\\'" . css-ts-mode)
         ;; XXX install https://github.com/mickeynp/html-ts-mode
         ;; ("\\.html\\'" . html-ts-mode)
         ("\\.py\\'" . python-ts-mode)
         ;;("\\.c\\'" . c-ts-mode)
         ;;("\\.h\\'" . c-ts-cpp)
         ;;("\\.mode\\'" . c++-ts-mode)
         ;;("\\.hpp\\'" . c++-ts-mode)
         ("\\.yml\\'" . yaml-ts-mode)
         ("\\.yaml\\'" . yaml-ts-mode)
         ("\\.toml\\'" . toml-ts-mode)
         )
  :init
  ;; XXX: it would be cool if i could install these via guix, but idk how...
  (defun m-setup-install-grammars ()
    "Install Tree-sitter grammars if they are absent."
    (interactive)
    (dolist (grammar
             '((css . ("https://github.com/tree-sitter/tree-sitter-css" "v0.20.0"))
               (bash "https://github.com/tree-sitter/tree-sitter-bash")
               (html . ("https://github.com/tree-sitter/tree-sitter-html" "v0.20.1"))
               (javascript . ("https://github.com/tree-sitter/tree-sitter-javascript" "v0.21.2" "src"))
               (json . ("https://github.com/tree-sitter/tree-sitter-json" "v0.20.2"))
               (python . ("https://github.com/tree-sitter/tree-sitter-python" "v0.20.4"))
               (go "https://github.com/tree-sitter/tree-sitter-go" "v0.20.0")
               (markdown "https://github.com/ikatyang/tree-sitter-markdown")
               (make "https://github.com/alemuller/tree-sitter-make")
               (elisp "https://github.com/Wilfred/tree-sitter-elisp")
               (cmake "https://github.com/uyha/tree-sitter-cmake")
               (c "https://github.com/tree-sitter/tree-sitter-c")
               (cpp "https://github.com/tree-sitter/tree-sitter-cpp")
               (c-sharp "https://github.com/tree-sitter/tree-sitter-c-sharp")
               (toml "https://github.com/tree-sitter/tree-sitter-toml")
               (tsx . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "tsx/src"))
               (typescript . ("https://github.com/tree-sitter/tree-sitter-typescript" "v0.20.3" "typescript/src"))
               (yaml . ("https://github.com/ikatyang/tree-sitter-yaml" "v0.5.0"))
               (prisma "https://github.com/victorhqc/tree-sitter-prisma")))
      (add-to-list 'treesit-language-source-alist grammar)
      ;; Only install `grammar' if we don't already have it
      ;; installed. However, if you want to *update* a grammar then
      ;; this obviously prevents that from happening.
      (unless (treesit-language-available-p (car grammar))
        (treesit-install-language-grammar (car grammar)))))

  ;; Optional, but recommended. Tree-sitter enabled major modes are
  ;; distinct from their ordinary counterparts.
  ;;
  ;; You can remap major modes with `major-mode-remap-alist'. Note
  ;; that this does *not* extend to hooks! Make sure you migrate them
  ;; also
  ;; This is a bit of a hack, and using the :mode above should be preferred
  (dolist (mapping
           '((python-mode . python-ts-mode)
             (css-mode . css-ts-mode)
             (typescript-mode . typescript-ts-mode)
             (js-mode . typescript-ts-mode)
             (js2-mode . typescript-ts-mode)
             ;;(c-mode . c-ts-mode)
             ;;(c++-mode . c++-ts-mode)
             ;;(c-or-c++-mode . c-or-c++-ts-mode)
             (bash-mode . bash-ts-mode)
             (css-mode . css-ts-mode)
             (json-mode . json-ts-mode)
             (js-json-mode . json-ts-mode)
             (sh-mode . bash-ts-mode)
             (sh-base-mode . bash-ts-mode)))
    (add-to-list 'major-mode-remap-alist mapping))
  :config
;; FIXME depends on g++ and gcc being installed
  (m-setup-install-grammars))

(use-package eglot
  :if (not m-ww?)
  :demand t
  :after (evil)
  :commands (eglot
             eglot-ensure
             eglot-rename
             eglot-format
             eglot-code-actions
             eglot-code-action-organize-imports
             eglot-code-action-quickfix
             eglot-code-action-extract
             eglot-code-action-inline
             eglot-code-action-rewrite
             eglot-find-declaration
             eglot-find-implementation)
  :init
  ;; disable events buffer for performance
  (setopt eglot-events-buffer-size 0)
  :config
  (m-code-def '(normal visual emacs)
    "r" #'eglot-rename
    "f" #'eglot-format
    "." #'eglot-code-actions
    "i" #'eglot-code-action-organize-imports
    "q" #'eglot-code-action-quickfix
    "e" #'eglot-code-action-extract
    "l" #'eglot-code-action-inline
    "w" #'eglot-code-action-rewrite
    "d" #'eldoc
    "D" #'eldoc-doc-buffer)
  (general-def 'normal
    "g D" #'eglot-find-declaration
    "g I" #'eglot-find-implementation)
  (setopt eglot-autoshutdown t ; auto shutdown language server if all its buffers are killed
          eglot-confirm-server-initiated-edits nil ; don't ask for confirmation when editing several files
          ))

(use-package eglot-booster
  :if (not m-ww?)
  :demand t
  :after (eglot)
  :commands 'eglot-booster-mode
  :init (eglot-booster-mode))

;; TODO: format in region maybe? doom has an implementation of this
(use-package apheleia
  :if (not m-ww?)
  :defer t
  :hook (prog-mode . apheleia-global-mode)
  :commands (apheleia-global-mode
             apheleia-format-buffer
             apheleia-mode)
  :diminish 'apheleia-mode
  :config
  (m-buffer-def '(emacs normal)
    "a" #'apheleia-format-buffer)
  (m-toggle-def '(normal emacs)
    "a" #'apheleia-mode))

(use-package editorconfig
  :if t
  :defer 1
  :commands (editorconfig-mode)
  :diminish 'editorconfig-mode
  :init
  (editorconfig-mode 1)
  (setopt editorconfig-trim-whitespaces-mode 'ws-butler-mode))

(use-package flycheck
  :if (not m-ww?)
  :defer 3
  :commands (global-flycheck-mode)
  :config
  (global-flycheck-mode)
  (m-code-def 'normal
    "e" (cons "flycheck" flycheck-command-map))
  ;; disable emacs-lisp-checkdoc because I tangle from org a lot
  (setopt flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(use-package electric
  :if t
  :defer 2
  :config
  ;; electric-pair-mode -- pairs like () and ""
  ;; (electric-pair-mode +1) ; dont enable globally
  (add-hook 'prog-mode-hook
            (defun m-enable-electric-pair-prog ()
              (electric-pair-local-mode +1)))
  ;; IDEA: consider smartparens instead
  ;; IDEA: better pairs in minibuffer (i.e. be smart in evil searching or pair in M-:)
  (add-hook 'minibuffer-setup-hook
            (defun m-disable-electric-pair-minibuffer ()
              (electric-pair-local-mode -1)))

  ;; for the weird european quote marks i think
  (electric-quote-mode -1)

  ;; auto indenting
  (electric-indent-mode +1)

  ;; auto adds newlines sometimes (i.e. after "()" in a c-like language function definition)
  ;; if enabling this, customize electric-layout-rules for each language
  ;; IDEA: make it so auto new line after typing (defun foo (arg)\n) in lisps/schemes/etc...
  (electric-layout-mode +1))

(use-package cc-mode
  :if t
  :defer 1
  :init
  (add-hook 'c-mode-hook #'eglot-ensure)
  (add-hook 'c++-mode-hook #'eglot-ensure)
  :config
  (add-to-list 'c-default-style '(c++-mode . "stroustrup"))
  (add-to-list 'c-default-style '(c-mode . "stroustrup"))
  (setq-default c-basic-offset 'set-from-style))

(use-package css-mode
  :if t
  :defer 1
  :config
  (setopt css-indent-offset 2))

(use-package typescript-ts-mode
  :if t
  :defer 1
  :init
  (setopt typescript-ts-mode-indent-offset 2))

(use-package easy-escape
  :disabled
  :if t
  :defer t
  :hook ((lisp-interaction-mode . easy-escape-minor-mode)
         (emacs-lisp-mode . easy-escape-minor-mode)))

(use-package eros
  :if t
  :defer 3
  :commands (eros-mode)
  :config
  (eros-mode +1))

(use-package highlight-defined
  :if t
  :defer t
  :hook ((lisp-interaction-mode . highlight-defined-mode)
         (emacs-lisp-mode . highlight-defined-mode)))

(use-package menu-bar
  :if t
  :demand t
  :config
  (m-toggle-def '(normal visual emacs)
    "d" #'toggle-debug-on-error))

(use-package thingatpt
  :if t
  :defer 3
  :config
  (defun m-symbol-file-at-point ()
    (interactive)
    (if-let* ((sym (thing-at-point 'symbol t))
              (file (symbol-file (intern sym))))
        (message (format "%s is in %s" sym (file-name-nondirectory file)))
      (message "no symbol at point found")))

  (m-mode-def '(normal emacs) emacs-lisp-mode-map
    "s" #'m-symbol-file-at-point)
  (m-mode-def '(normal emacs) lisp-interaction-mode-map
    "s" #'m-symbol-file-at-point))

;; (use-package stumpwm-mode)

(use-package sly
  :unless (or m-ww? m-wl?)
  :defer 1)

(use-package scheme
  :if (or m-eq? m-su?)
  :defer 1)

(use-package geiser
  :if (or m-eq? m-su?)
  :defer 2)

(use-package geiser-guile
  :if (or m-eq? m-su?)
  :defer 1
  :config
  (add-to-list 'geiser-guile-load-path "~/sfs/proj/guix"))

(use-package mu4e
  :if (or m-eq? m-su?)
  :defer 20 ; Wait until 20 seconds after startup
  :commands (make-mu4e-context
             mu4e-message-field
             mu4e)
  :config
  (setopt mail-user-agent 'mu4e-user-agent
          message-confirm-send t

          ;; HACK:
          ;; https://github.com/djcb/mu/issues/2214
          ;; Setting this to nil for now because it breaks the \SEEN flag from dynadot.
          ;; I think this is fundamentally an issue in mbsync, but this is an ok workaround.
          ;; This is weird because the documentation indicates this should be `t' when using
          ;; mbsync.
          ;; changing back to t idfk
          mu4e-change-filenames-when-moving t

          mu4e-get-mail-command "mbsync -a" ; used for manual refreshes
          mu4e-update-interval (* 10 60)
          message-send-mail-function 'smtpmail-send-it
          mu4e-context-policy 'pick-first ; pick first (default) context
          mu4e-compose-context-policy 'ask

          ;; XXX: set to `t' to make things look good in non plain text. doesn't seem to be working
          mu4e-compose-format-flowed t)

  (setopt mu4e-contexts
          (list
           ;; master account
           (make-mu4e-context
            :name "mail"
            :match-func
            (lambda (msg)
              (when msg
                (string-prefix-p "/private/mail" (mu4e-message-field msg :maildir))))
            :vars '((user-mail-address . "mail@marcusquincy.org")
                    (user-full-name    . "Marcus Quincy")
                    (smtpmail-smtp-server  . "mail.privateemail.com")
                    (smtpmail-smtp-service . 465)
                    (smtpmail-stream-type  . ssl)
                    (smtpmail-smtp-user  . "mail@marcusquincy.org")
                    ;; folders relative to results of (mu4e-root-maildir)
                    ;; which was set in mu init command (run `mu info`)
                    (mu4e-drafts-folder  . "/Drafts")
                    (mu4e-sent-folder  . "/Sent")
                    (mu4e-refile-folder  . "/Archive")
                    (mu4e-trash-folder  . "/Trash")
                    (mu4e-compose-signature . "Marcus Quincy\n"))))))

;; Make gc pauses faster by decreasing the threshold.
(setq gc-cons-threshold (* 2 1000 1000))
