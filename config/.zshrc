autoload -Uz compinit promptinit
compinit
promptinit

# tab completions will include dotfiles
# alternatively use `setopt globdots`, but that also effects commands like rm *
_comp_options+=(globdots)
# highlight selections during tab complete
zstyle ':completion:*' menu select
# make completion case insensitive
# https://superuser.com/questions/1092033/how-can-i-make-zsh-tab-completion-fix-capitalization-errors-for-directories-and
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'

# # TODO: play with the following:
# # https://unix.stackexchange.com/questions/214657/what-does-zstyle-do
# # Color completion for some things.
# # http://linuxshellaccount.blogspot.com/2008/12/color-completion-using-zsh-modules-on.html
# zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
# # formatting and messages
# # http://www.masterzen.fr/2009/04/19/in-love-with-zsh-part-one/
# zstyle ':completion:*' verbose yes
# zstyle ':completion:*:descriptions' format "$fg[yellow]%B--- %d%b"
# zstyle ':completion:*:messages' format '%d'
# zstyle ':completion:*:warnings' format "$fg[red]No matches for:$reset_color %d"
# zstyle ':completion:*:corrections' format '%B%d (errors: %e)%b'
# zstyle ':completion:*' group-name ''

# # Completers for my own scripts
# zstyle ':completion:*:*:sstrans*:*' file-patterns '*.(lst|clst)'
# zstyle ':completion:*:*:ssnorm*:*' file-patterns '*.tsv'

# typing the name of a directory without cd will cd into it
setopt autocd

# if the value of a variable is a directory, it will work with autocd
setopt cdablevars

# spell correction for commands and arguments
# setopt correct
# setopt correctall

# allow comments in interactive prompt
setopt interactivecomments

# configure vi-mode, i.e. put mode in prompt
# https://github.com/jeffreytse/zsh-vi-mode?tab=readme-ov-file#vi-mode-indicator
ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT

ZVM_PROMPT="%F{green}λ%f"
function zvm_after_select_vi_mode() {
    if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
        # set variable in Emacs so it knows current mode
        case $ZVM_MODE in
            $ZVM_MODE_NORMAL)
                vterm_cmd m-zsh-mode-set-normal
                ;;
            $ZVM_MODE_INSERT)
                vterm_cmd m-zsh-mode-set-insert
                ;;
            $ZVM_MODE_VISUAL)
                vterm_cmd m-zsh-mode-set-visual
                ;;
            $ZVM_MODE_VISUAL_LINE)
                vterm_cmd m-zsh-mode-set-visual-line
                ;;
            $ZVM_MODE_REPLACE)
                vterm_cmd m-zsh-mode-set-replace
                ;;
        esac
    fi

    case $ZVM_MODE in
        $ZVM_MODE_NORMAL)
            ZVM_PROMPT="%F{yellow}λ%f"
            ;;
        $ZVM_MODE_INSERT)
            ZVM_PROMPT="%F{green}λ%f"
            ;;
        $ZVM_MODE_VISUAL)
            ZVM_PROMPT="%F{cyan}λ%f"
            ;;
        $ZVM_MODE_VISUAL_LINE)
            ZVM_PROMPT="%F{blue}λ%f"
            ;;
        $ZVM_MODE_REPLACE)
            ZVM_PROMPT="%F{white}λ%f"
            ;;
    esac
}

# set the prompt
#see
#https://dev.to/voracious/a-guide-to-customizing-the-zsh-shell-prompt-2an6
#https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
#https://zsh.sourceforge.io/Doc/Release/User-Contributions.html#vcs_005finfo-Configuration
parse_git_dirty() {
    git_status="$(timeout 0.1s git status 2> /dev/null)"
    [[ "$git_status" =~ "Changes to be committed:" ]] && echo -n "%F{green}·%f"
    [[ "$git_status" =~ "Changes not staged for commit:" ]] && echo -n "%F{yellow}·%f"
    [[ "$git_status" =~ "Untracked files:" ]] && echo -n "%F{red}·%f"
}

setopt prompt_subst

NEWLINE=$'\n'

autoload -Uz vcs_info # enable vcs_info
precmd () { vcs_info } # always load before displaying the prompt
zstyle ':vcs_info:git*' formats ' ↣ (%F{254}%b%F{245})' # format $vcs_info_msg_0_

GUIX_ENV=''
if [ -n "$GUIX_ENVIRONMENT" ]
then
    GUIX_ENV='[env] '
fi

PS1='${GUIX_ENV}%F{cyan}%n%F{245} ↣ %F{cyan}%(5~|%-1~/⋯/%3~|%4~)%F{245}${vcs_info_msg_0_} $(parse_git_dirty)$NEWLINE%(?..%F{red}[%?]%f)${ZVM_PROMPT} '
RPS1='%F{cyan}!%!%F{245} @%*' # show history number and timestamp on rhs


# emacs bindings
bindkey -e

# history settings
export HISTFILE=$ZDOTDIR/.histfile
export HISTSIZE=100000   # the number of items for the internal history list
export SAVEHIST=100000   # maximum number of items for the history file
# The meaning of these options can be found in man page of `zshoptions`.
setopt HIST_REDUCE_BLANKS  # remove unnecessary blanks
setopt INC_APPEND_HISTORY_TIME  # append command to history file immediately after execution
setopt EXTENDED_HISTORY  # record command start time
# setopt histignoredups # dont add repeat commands to histfile
setopt histignorespace # dont add commands starting with space to histfile


# vterm integration
if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
    # manages escape sequences
    function vterm_printf() {
        if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ]); then
            # Tell tmux to pass the escape sequences through
            printf "\ePtmux;\e\e]%s\007\e\\" "$1"
        elif [ "${TERM%%-*}" = "screen" ]; then
            # GNU screen (screen, screen-256color, screen-256color-bce)
            printf "\eP\e]%s\007\e\\" "$1"
        else
            printf "\e]%s\e\\" "$1"
        fi
    }

    # use vterm_cmd so send commands to Emacs. This automates escape character substitution.
    function vterm_cmd() {
        local vterm_elisp
        vterm_elisp=""
        while [ $# -gt 0 ]; do
            vterm_elisp="$vterm_elisp""$(printf '"%s" ' "$(printf "%s" "$1" | sed -e 's|\\|\\\\|g' -e 's|"|\\"|g')")"
            shift
        done
        vterm_printf "51;E$vterm_elisp"
    }

    # make clear work in vterm
    alias clear='vterm_printf "51;Evterm-clear-scrollback";tput clear'

    # HACK this fucks up the zsh-auto-complete because it redefines add-zsh-hook.
    # see https://github.com/akermu/emacs-libvterm/issues/574
    # so we apply the same workaround as https://github.com/pkgxdev/pkgx/issues/281
    # autoload -U add-zsh-hook
    # # make vterm title pwd
    # add-zsh-hook -Uz chpwd (){ print -Pn "\e]2;%2~\a" }
    function vterm_set_pwd() {
        print -Pn "\e]2;%2~\a"
    }
    typeset -ag chpwd_functions;

    if [[ -z "${chpwd_functions[(r)vterm_set_pwd]+1}" ]]; then
        chpwd_functions=( vterm_set_pwd ${chpwd_functions[@]} )
    fi

    vterm_prompt_end() {
        vterm_printf "51;A$(whoami)@$(cat /etc/hostname):$(pwd)"
    }
    setopt PROMPT_SUBST
    PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'
fi

# source brew if it exists
if [ -f /home/linuxbrew/.linuxbrew/bin/brew ]; then
    eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
fi


# aliases
alias ls='ls -p --color=auto'
alias ll='ls -l'
alias la='ls -la'
alias grep='grep -n --color=auto'
# show all the history stored. (16 default)
alias history="fc -l 1"
alias ...="cd ../.."
alias ....="cd ../../.."

mkcd () {
    mkdir -p $1 && cd $1
}

cl () {
    cd $1 && ls -p --color=auto
}

gshell () {
    guix shell --pure zsh coreutils-minimal $@ -- zsh
}

ed () {
    if [[ "$INSIDE_EMACS" = 'vterm' ]]; then
        vterm_cmd find-file "$(realpath "${@:-.}")"
    else
        emacsclient -nw --alternate-editor="" $@
    fi
}
