// See:
// https://github.com/felixc/dotfiles/blob/eaab386da72003b6fc188fdcbc1a81e2d5d44da8/moz-user.js
// https://felixcrux.com/blog/keeping-firefox-settings-in-a-config-file
// look in the .mozilla/firefox/XXXXX.default-release/prefs.js file to see what the browser generates for these
// https://support.mozilla.org/en-US/kb/customizing-firefox-using-autoconfig
// maybe look at https://github.com/arkenfox/user.js for privacy stuff

// be strict for blocking cookies
user_pref("browser.contentblocking.category", "strict");

// opt out of studies
user_pref("app.shield.optoutstudies.enabled", false);

// dont show sponsored/clickbait things
user_pref("browser.newtabpage.activity-stream.showSponsored", false);
user_pref("browser.newtabpage.activity-stream.showSponsoredTopStories", false);
user_pref("browser.newtabpage.activity-stream.feeds.section.topstories", false);

// try to just use https
user_pref("dom.security.https_only_mode", true);

// fingerprints scary
user_pref("privacy.fingerprintingProtection", true);


// Blank home page
user_pref("browser.startup.homepage", "about:newtab");

// Re-open tabs when launching the browser
user_pref("browser.startup.page", 3);

// Always ask where to download files to
user_pref("browser.download.useDownloadDir", false);

// Open new windows in new tabs
user_pref("browser.link.open_newwindow", 3);

// Don't warn when opening multiple tabs
user_pref("browser.tabs.warnOnOpen", false);

// Don't load tabs until switching to them
user_pref("browser.sessionstore.restore_on_demand", true);

// Don't switch to new tabs upon opening them
user_pref("browser.tabs.loadInBackground", true);

// Don't accidentally quit the whole browser when hitting Ctrl-Q
user_pref("browser.quitShortcut.disabled", true);

// Don't show suggested sites in the New Tab page
user_pref("browser.newtabpage.enhanced", false);

// Search NOT using Google
user_pref("browser.search.defaultenginename", "DuckDuckGo");
user_pref("browser.urlbar.placeholderName", "DuckDuckGo");
user_pref("browser.urlbar.placeholderName.private", "DuckDuckGo");
user_pref("browser.newtabpage.activity-stream.improvesearch.topSiteSearchShortcuts.havePinned", ""); // was google and amazon
user_pref("browser.newtabpage.pinned", "[]"); // was google and amazon

// Don't recommend search suggestions
user_pref("browser.search.suggest.enabled", false);

// Accepted languages
user_pref("intl.accept_languages", "en-us,en-ca,en-gb,en");

// Send Do Not Track header
user_pref("privacy.donottrackheader.enabled", true);

// Use Tracking Protection mode everywhere, not just in Private Browsing
user_pref("privacy.trackingprotection.enabled", true);

// Don't send telemetry or health reports
user_pref("datareporting.healthreport.uploadEnabled", false);
user_pref("datareporting.policy.dataSubmissionEnabled", false);

// Don't allow use of the Beacon APIs
user_pref("beacon.enabled", false);

// Accept cookies, but third-party ones only from sites we've visited
user_pref("network.cookie.cookieBehavior", 3);

// Don't remember form history
user_pref("browser.formfill.enable", false);

// Don't try to guess TLDs for things entered in the address bar
user_pref("browser.fixup.alternate.enabled", false);

// Don't force a wait before installing an addon
user_pref("security.dialog_enable_delay", 0);

// Don't warn about making changes in about:config
user_pref("browser.aboutConfig.showWarning", false);
