#!/usr/bin/env python3

import socket
#IS_GUIX = socket.gethostname() == "equanimity"
IS_PERSONAL = socket.gethostname() in ["equanimity", "sublimity"]

# if IS_GUIX:
#     c.qt.args.append("disable-seccomp-filter-sandbox") # needed for text to render on guix


c.fonts.default_size = "16pt"
c.statusbar.position = "top"
c.hints.chars = "ineahtsr"
c.auto_save.session = True
c.colors.webpage.darkmode.enabled = True
if IS_PERSONAL:
  c.colors.webpage.darkmode.policy.images = "smart-simple"
else:
  c.colors.webpage.darkmode.policy.images = "smart"
c.colors.webpage.preferred_color_scheme = 'dark'

#config.set('content.headers.user_agent', 'Mozilla/5.0 ({os_info}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99 Safari/537.36', 'https://*.slack.com/*')

config.unbind('<Ctrl-V>', mode='normal') #unbind C-v, so it will paste instead of passthrough-mode
config.bind('<Ctrl-Shift-V>', 'mode-enter passthrough', mode='normal') # use C-S-v to get into passthrough instead
config.bind('<Ctrl-Shift-V>', 'mode-leave', mode='passthrough')
config.bind(',d', 'config-cycle colors.webpage.darkmode.enabled ;; restart')
config.bind(',r', 'config-source') # source the config file
config.bind(',m', 'spawn mpv {url}') # MPV
config.bind(',M', 'hint links spawn mpv {hint-url}')

# binding to add domain to "trusted" domains
config.bind(',C', 'set -p -u {url:domain} content.javascript.clipboard access')

# binding to enable clipboard access for 3s
config.bind(',c', 'set -t content.javascript.clipboard access ;; cmd-later 3s set -p content.javascript.clipboard none')

# give qutebrowser emacs-like bindings in insert mode, thanks Gavin!
# https://www.reddit.com/r/qutebrowser/comments/hcn2e5/what_are_your_favorite_custom_qutebrowser/
# config.bind('<Ctrl-h>', 'fake-key <Backspace>', 'insert')
# config.bind('<Ctrl-a>', 'fake-key <Home>', 'insert')
# config.bind('<Ctrl-e>', 'fake-key <End>', 'insert')
# config.bind('<Ctrl-b>', 'fake-key <Left>', 'insert')
# config.bind('<Mod1-b>', 'fake-key <Ctrl-Left>', 'insert')
# config.bind('<Ctrl-f>', 'fake-key <Right>', 'insert')
# config.bind('<Mod1-f>', 'fake-key <Ctrl-Right>', 'insert')
# config.bind('<Ctrl-p>', 'fake-key <Up>', 'insert')
# config.bind('<Ctrl-n>', 'fake-key <Down>', 'insert')
# config.bind('<Mod1-d>', 'fake-key <Ctrl-Delete>', 'insert')
# config.bind('<Ctrl-d>', 'fake-key <Delete>', 'insert')
# config.bind('<Ctrl-w>', 'fake-key <Ctrl-Backspace>', 'insert')
# config.bind('<Ctrl-u>', 'fake-key <Shift-Home><Delete>', 'insert')
# config.bind('<Ctrl-k>', 'fake-key <Shift-End><Delete>', 'insert')
config.bind('<Ctrl-x><Ctrl-e>', 'open-editor', 'insert')

# block yt ads (also from gavin)
# DISCLAIMER: I did not make this myself I found it in someone elses config a while back
from qutebrowser.api import interceptor
def filter_yt(info: interceptor.Request):
  """Block the given request if necessary."""
  url = info.request_url
  if (url.host() == 'www.youtube.com' and
      url.path() == '/get_video_info' and
      '&adformat=' in url.query()):
    info.block()
    interceptor.register(filter_yt)

# load the autoconfig file so i can use :set
config.load_autoconfig(True)
