;;; -*- mode: lisp; eval: (auto-fill-mode +1) -*-


#|
NOTES
hackable common lisp window manager
[[info:stumpwm][info stumpwm]]

- start the slynk server with C-t ; slynk RET
- connect to the sly server with M-x sly-connect
- to jump to get help, press C-t h (*HELP-MAP*)
- to go to a func or command definition, I can call that command from
lisp and then use gd to goto definition.

TODO: reference: https://github.com/logoraz/guix-craft

|#


;;; initial setup
(in-package :stumpwm) ; declare in stumpwm so we don't prefix with stumpwm:

;; set load path to packages installed via guix
(init-load-path #p"~/.guix-home/profile/share/common-lisp/sbcl/")



;;; font
;; I had a hard time getting font to work. I had to install a bunch of
;; dependencies through guix.  Config adapted from
;; https://guix.gnu.org/cookbook/en/html_node/StumpWM.html and
;; https://github.com/Gavinok/stump-conf/blob/main/config
(load-module "ttf-fonts")

;; add guix fonts
(pushnew (concat (getenv "HOME")
                 "/.guix-home/profile/share/fonts/truetype/")
         xft:*font-dirs* :test #'string=)

;; set cache
(setf clx-truetype:+font-cache-filename+ (concat (getenv "HOME")
                                                 "/.fonts/font-cache.sexp"))
(xft:cache-fonts)

(set-font (make-instance 'xft:font
                         :family "Fira Code"
                         :subfamily "Regular"
                         :size 12))


;;; REPL
(require :slynk)
(defcommand slynk (port) ((:string "Port number: "))
  (sb-thread:make-thread
   (lambda ()
     (slynk:create-server :port (parse-integer port) :dont-close t))
   :name "slynk-manual"))


;;; config
(setf *mouse-focus-policy* :click)
(setf *message-window-gravity* :center)
(setf *input-window-gravity* :center)

;; floating groups/windows
(setf *float-window-modifier* :super)

;; dynamic groups
;; stay on master, follow if stack
(setf *rotation-focus-policy* :master-or-follow)

;; run or raise should be local to group
(setf *run-or-raise-all-groups* nil)



;;; keybindings
;; TODO: do a lot more than this...
;; *root-map* is what C-t is bound to
;; *top-map* has no prefix

;; put root-map on s-c instead of C-t to prevent conflicts
(set-prefix-key (kbd "s-c"))

(defvar *xorg-map* (make-sparse-keymap))

(define-key *top-map* (kbd "s-SPC") "windowlist")
(loop for i upto 9 do
  (define-key *top-map* (kbd (format nil "s-~D" i))
    (format nil "select-window-by-number ~D" i)))

;; keys to open my common applications quickly
;; (defcommand emacs () ()
;;   "Start emacs unless it is already running, in which case focus it."
;;   (run-or-raise "emacs" '(:class "Emacs")))
(define-key *top-map* (kbd "s-e") "emacs")

(defcommand qutebrowser () ()
  "start or focus qutebrowser"
  (run-or-raise "qutebrowser" '(:class "qutebrowser")))
(define-key *top-map* (kbd "s-q") "qutebrowser")

(defcommand firefox () ()
  "start or focus firefox"
  (run-or-raise "firefox" '(:class "firefox")))
(define-key *top-map* (kbd "s-f") "firefox")

(defcommand st () ()
  "start or focus st"
  (run-or-raise "st -e zsh" '(:class "st-256color")))
(define-key *top-map* (kbd "s-t") "st")

;; make C-tab and C-S-tab work in tmux
(define-remapped-keys
    '(("st-256color"
       ("C-TAB"   . ("C-x" "n"))
       ("C-ISO_Left_Tab"   . ("C-x" "p")))))

;; remove all windows from frame showing the bg
(define-key *root-map* (kbd "-") "fclear")

;; navigate spatially with vim-like bindings
(define-key *top-map* (kbd "s-h") "move-focus left")
(define-key *top-map* (kbd "s-j") "move-focus down")
(define-key *top-map* (kbd "s-k") "move-focus up")
(define-key *top-map* (kbd "s-l") "move-focus right")

(define-key *top-map* (kbd "s-H") "move-window left")
(define-key *top-map* (kbd "s-J") "move-window down")
(define-key *top-map* (kbd "s-K") "move-window up")
(define-key *top-map* (kbd "s-L") "move-window right")


(defcommand help-top-map () ()
  "shows all keybindings in *top-map*"
  (apply 'display-bindings-for-keymaps nil (dereference-kmaps '(*top-map*))))
(define-key *top-map* (kbd "s-?") "help-top-map")

;; xorg bindings
(define-key *top-map* (kbd "s-X") *xorg-map*)

(defcommand xmodmap () ()
  "re-run xmodmap"
  (run-shell-command "xmodmap ~/.Xmodmap"))
(define-key *xorg-map* (kbd "k") "xmodmap")

(defcommand xsetroot () ()
  "re-run xsetroot"
  (run-shell-command "xsetroot -cursor_name left_ptr"))
(define-key *xorg-map* (kbd "m") "xsetroot")

(defcommand mlock () ()
  "lock screen"
  (run-shell-command "mlock"))
(define-key *xorg-map* (kbd "m") "mlock")

(defcommand hdmi () ()
  "show all output dp2"
  (run-shell-command "xrandr --output eDP-1 --off --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --primary --mode 3440x1440 --pos 0x0 --rotate normal"))
(define-key *xorg-map* (kbd "h") "hdmi")

(defcommand edp1 () ()
  "show all output edp1"
  (run-shell-command "xrandr --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1 --off --output HDMI-1 --off --output DP-2 --off --output HDMI-2 --off"))
(define-key *xorg-map* (kbd "e") "edp1")


;; VOLUME
;; FIXME is this macro the reason why the keymap is so slow?
(defmacro define-non-passthrough-map
    (name (&key on-enter on-exit abort-if (exit-on '((kbd "RET")
                                                     (kbd "ESC")
                                                     (kbd "C-g"))))
     &body key-bindings)
  (let ((used-keys (append (loop for keyb in exit-on
                                 collect (cadr keyb))
                           (loop for keyb in key-bindings
                                 collect (cadar keyb)))))
    `(define-interactive-keymap ,name
         (:on-enter ,on-enter
          :on-exit ,on-exit
          :abort-if ,abort-if
          :exit-on ,exit-on)
       ,@key-bindings
       ,@(loop for i from 32 to 122
               append
               (remove-if-not
                #'identity
                (loop for mod in '("C-" "M-" "s-" "S-" "")
                      collect
                      (let* ((ch (code-char i))
                             (kb (format nil "~A~A"
                                         mod
                                         (cond
                                           ((equal ch #\ ) "SPC")
                                           (t ch)))))
                        (unless (member kb used-keys :test #'string=)
                          `((kbd ,kb) ,(format nil "~A:~A" name kb))))))))))


(load-module "pamixer")

(setf pamixer:*step* 10) ; j and k increment by 10

(setf pamixer:*source-modeline-fmt* "%v") ; just show % for mic, not bar

;; use %S in modeline for source volume
(add-screen-mode-line-formatter #\S 'pamixer:source-modeline)

;; i think stumpwm:bar is bugged, doesn't have color termination char
;; or perhaps pamixer:ml-bar should have a color terminator in it's concat...
;; Here i redefine the pamixer:ml-bar function to include the
;; termination char
(defun pamixer:ml-bar (volume muted)
  (concat "\["
          (stumpwm:bar (if muted 0 (min 100 volume)) 5 #\X #\=)
          "^n\]"))

(defvar pamixer-map-active nil)
(defvar pamixer-map-help-string
  (concat
   "^1PAMIXER^n %P %S -- [m]ute toggle, [j] volume down, "
   "[k] volume up, [s]et volume, [M]ute mic, "
   "[J] mic vol down, [K] mic vol up, [S]et mic"))
(define-non-passthrough-map pamixer-map
    (:on-enter (lambda ()
                 (setf pamixer-map-active t)
                 (update-mode-lines (current-screen)))
     :on-exit (lambda ()
                (setf pamixer-map-active nil)
                (update-mode-lines (current-screen))))
  ;; t at end means exit keymap immediately after running command
  ((kbd "m") "pamixer-toggle-mute" t)
  ((kbd "j") "pamixer-volume-down")
  ((kbd "k") "pamixer-volume-up")
  ((kbd "s") "pamixer-set-volume" t)
  ((kbd "M") "pamixer-source-toggle-mute" t)
  ((kbd "J") "pamixer-source-volume-down")
  ((kbd "K") "pamixer-source-volume-up")
  ((kbd "S") "pamixer-source-set-volume" t))
(define-key *top-map* (kbd "s-v") "pamixer-map")



;;; modeline
;; TODO gain feature parity with what I had in sway (battery, gpg, volume, networking, brightness)

(load-module "battery-portable")

(setf *window-format* "%m%n%s%c")
;; %B is battery %P is pamixer
(setf *screen-mode-line-format*
      (list
       '(:eval (cond
                 (pamixer-map-active pamixer-map-help-string)
                 (t "[^B%n^b] %B %P %S %W^>%d")))))
(setf *time-modeline-string* "%a %b %e %k:%M")
(setf *mode-line-timeout* 60) ; update every 60s

;; enable modeline. must be after modeline options.
(enable-mode-line (current-screen) (current-head) t)

;; TODO add system tray

;;; startup funcs
(defun startup ()
  (xmodmap)
  (xsetroot)
  (run-shell-command "xss-lock -- mlock"))

(startup)
