* Stump modules

musts:

- [X] ttf-fonts
- [X] *pamixer* OR amixer OR volume-control
  - pamixer most featureful, but i don't currently use pulseaudio, not sure if problem?
- [ ] acpi-backlight OR stump-backlight
- [X] battery-portable
- [ ] net OR wifi OR stump-nm
- [ ] stumptray
- [ ] beckon
- [ ] clipboard-history
- [ ] command-history
- [ ] pinentry
- [ ] screenshot
- [X] +stump-lock+ using i3 lock instead.

maybes:

- [ ] weather
- [ ] dmenu
- [ ] cpu
- [ ] maildir
- [ ] mem
- [ ] notifications OR alert-me OR notify
  - [ ] i.e. integrate with org-agenda?
- [ ] end-session
- [ ] swm-pomodoro
- [ ] swm-gaps
- [ ] windowtags
- [ ] winner-mode
