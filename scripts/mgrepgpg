#!/bin/bash
# https://gist.github.com/pachanka/7928b484d70882cd69f3119d5585305a
# Usage search-with-gpg path/to/encrypted/files/*
#

if [ -z "$1" ]; then
    echo "Usage: $0 'search string' [path/to/encrypted/files/*]";
    exit 1;
else
    SEARCH=$1;
fi

read -rsp "Enter passphrase: " PASSPHRASE
echo "";
TMPDIR="/tmp/${0##*/}-$$";

if [ -d "$TMPDIR" ]; then
    if [ ! -w "$TMPDIR" ]; then
        echo "Error: cannot delete $TMPDIR"
        exit 1;
    else
        rm -rf $TMPDIR
    fi
fi

mkdir -v "$TMPDIR";

echo "Searching for $SEARCH ...";

for F in "${@:2}"
do
    TMPFILE="${TMPDIR}/${F##*/}"
    echo "$PASSPHRASE" | gpg --quiet --passphrase-fd 0 --batch -d --output "${TMPFILE}.txt" "${F}" > /dev/null
done

if [ "$(ls -A $TMPDIR)" ]; then
     grep --color=always -rni "$SEARCH" $TMPDIR

    # Shred files
    if type shred > /dev/null ; then
        # shred is present in PATH
        find ${TMPDIR} -type f -exec shred {} \;
    fi

    rm -rf ${TMPDIR}

else
    echo "Error decrypting files.";
fi

exit 1;
