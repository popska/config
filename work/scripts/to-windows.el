;;; -*- lexical-binding: t -*-

;; /mnt/c/Users/$USER/ will be my ~
(defconst m-home (expand-file-name (getenv "USER") "/mnt/c/Users/"))

;; copy init.el and lisp dir
(progn
  (copy-file (expand-file-name "init.el" "~/sfs/dotfiles/emacs/") (expand-file-name ".emacs.d/" m-home) t)
  (seq-do (lambda (file)
            (unless (file-directory-p file)
              (copy-file file
                         (expand-file-name
                          (file-name-nondirectory file)
                          (expand-file-name ".emacs.d/lisp" m-home))
                         t)))
          (directory-files (expand-file-name "lisp" "~/sfs/dotfiles/emacs/") t)))

;; copy over everything in load-path
(seq-do (lambda (dir-path)
          (unless (string-match-p ".config/emacs/lisp" dir-path)
            (ignore-errors
              (copy-directory dir-path (expand-file-name ".emacs.d/guix-packages/" m-home) t t nil))))
        load-path)

;; # paths that need to be transferred from wsl to windows
;; # - bashrc
;; # - qutebrowser.py
;; # - vimrc
