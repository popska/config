(use-modules (gnu home))

(include "./utils.scm")
(include "./emacs-packages.scm")
(include "./definitions.scm")

(home-environment
 (packages (append equanimity-packages
                   personal-packages
                   common-packages
                   personal-emacs-packages
                   common-emacs-packages))

 (services
  (list
   (make-bash-service)
   (make-zsh-service)
   (make-channels-service)
   (make-home-files-service (append (personal-homefiles (append equanimity-alist))
                                    (common-homefiles (append common-alist))))
   (make-env-vars-service (append personal-env-vars common-env-vars))
   (make-cron-service)
   (make-batsignal-service)
   (make-gpg-service)
   (make-home-activation-service write-channels-to-file-gexp)
   (make-emacs-daemon-service)
   (make-syncthing-service))))
