(use-modules (gnu home))

;; TODO: can probably move the (include utils.scm) to definitions.scm
(include "./utils.scm")
(include "./emacs-packages.scm")
(include "./definitions.scm")

(home-environment
 (packages (append work-packages
                   common-packages
                   work-emacs-packages
                   common-emacs-packages))

 (services
  (list
   (make-bash-service)
   (make-zsh-service)
   (make-channels-service)
   (make-home-files-service (common-homefiles (append common-alist)))
   (make-env-vars-service common-env-vars)
   (make-cron-service)
   (make-home-activation-service write-channels-to-file-gexp)
   (make-emacs-daemon-service)
   ;; enable once home template service is working correctly
   #;(make-home-template-service))))
