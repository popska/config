;;; -*- buffer-read-only: t; -*-

;; [[file:../emacs/emacs.org::*Package Management][Package Management:2]]
(use-modules
 (guix packages) ; provides (package)
 (guix git-download) ; provides (git-fetch)
 (guix build-system emacs) ; provides emacs-build-system
 (gnu packages emacs-xyz) ; provides all variables in emacs-xyz
 ((guix licenses) #:prefix license:))

;; [[file:emacs.org::*Package Management][]]
(define-public emacs-evil-fringe-mark
  (package
   (name "emacs-evil-fringe-mark")
   (version "1.2.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/Andrew-William-Smith/evil-fringe-mark")
           (commit "a1689fddb7ee79aaa720a77aada1208b8afd5c20")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0pf8bl7bmcn1l0dlxkgbgwb1n2fxpzxwcr4jf06gzyrrmmwah526"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-evil emacs-fringe-helper emacs-goto-chg))
   (home-page "https://github.com/Andrew-William-Smith/evil-fringe-mark")
   (synopsis "...")
   (description "...")
   (license license:gpl3)))
;; ends here
;; [[file:emacs.org::*Package Management][]]
;; modus themes locked into 4.2
(define emacs-modus-themes
  (package
    (name "emacs-modus-themes")
    (version "4.2.0")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://git.sr.ht/~protesilaos/modus-themes")
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1r6m2jsfn6066155pnlkdgs6dz2fdsampdhdz796z2jy53k7srsg"))))
    (native-inputs (list texinfo))
    (build-system emacs-build-system)
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'makeinfo
            (lambda _
              (invoke "emacs"
                      "--batch"
                      "--eval=(require 'ox-texinfo)"
                      "--eval=(find-file \"doc/modus-themes.org\")"
                      "--eval=(org-texinfo-export-to-info)")
              (install-file "doc/modus-themes.info"
                            (string-append #$output "/share/info")))))))
    (home-page "https://protesilaos.com/modus-themes/")
    (synopsis "Accessible themes for Emacs (WCAG AAA standard)")
    (description
     "The Modus themes are designed for accessible readability.  They conform
with the highest standard for color contrast between any given combination of
background and foreground values.  This corresponds to the WCAG AAA standard,
which specifies a minimum rate of distance in relative luminance of 7:1.

The Modus themes consist of six themes.  Modus Operandi is a light theme,
while Modus Vivendi is dark.  Modus Operandi Tinted and Modus Vivendi Tinted
are variants of the two main themes.  They slightly tone down the intensity of
the background and provide a bit more color variety.  Modus Operandi
Deuteranopia and its companion Modus Vivendi Deuteranopia are optimized for
users with red-green color deficiency.")
    (license (list license:gpl3+
                   license:fdl1.3+)))) ; GFDLv1.3+ for the manual
;; ends here
;; [[file:emacs.org::*Package Management][]]
(define-public emacs-consult-todo
  (package
   (name "emacs-consult-todo")
   (version "0.4.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/liuyinz/consult-todo")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0v336l9dary68i910yvpk9c24b9vrc1cx615hiv9dz8zi1khz8rr"))))
   (build-system emacs-build-system)
   (propagated-inputs (list emacs-consult emacs-hl-todo))
   (home-page "https://github.com/liuyinz/consult-todo")
   (synopsis "...")
   (description "...")
   (license license:gpl3)))
;; ends here
;; [[file:emacs.org::*Package Management][]]
(define-public emacs-nerd-icons-dired
  (let ((commit "c0b0cda2b92f831d0f764a7e8c0c6728d6a27774"))
    (package
     (name "emacs-nerd-icons-dired")
     (version "0.0.1")
     (source
      (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/rainstormstudio/nerd-icons-dired.git")
             (commit commit)))
       (file-name (git-file-name name version))
       (sha256
        (base32 "1iwqzh32j6fsx0nl4y337iqkx6prbdv6j83490riraklzywv126a"))))
     (build-system emacs-build-system)
     ;; is propagated inputs right?
     (propagated-inputs (list emacs-nerd-icons))
     (home-page "https://github.com/rainstormstudio/nerd-icons-dired.git")
     (synopsis "...")
     (description "...")
     (license license:gpl3))))
;; ends here
;; [[file:emacs.org::*Package Management][]]
(define-public emacs-evil-collection
  (package
   (name "emacs-evil-collection")
   (version "0.0.10")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/emacs-evil/evil-collection")
           (commit "5a1315bfc7f21533de12f35544b29c29ccec491d")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0f6kw8fdbyhh5a03p6j4w60229pl3zmcdq4z9fa3cp225kkp1dwr"))))
   (build-system emacs-build-system)
   (arguments
    (list
     #:include #~(cons* "^modes\\/" %default-include)
     #:tests? #true
     #:test-command #~(list "emacs" "-Q" "--batch"
                            "-L" "."
                            "-L" "./test"
                            "-l" "evil-collection-test.el"
                            "-l" "evil-collection-magit-tests.el"
                            "-f" "ert-run-tests-batch-and-exit")))
   (native-inputs
    (list emacs-magit))
   (propagated-inputs
    (list emacs-annalist emacs-evil))
   (home-page "https://github.com/emacs-evil/evil-collection")
   (synopsis "Collection of Evil bindings for many major and minor modes")
   (description "This is a collection of Evil bindings for the parts of
Emacs that Evil does not cover properly by default, such as @code{help-mode},
@code{M-x calendar}, Eshell and more.")
   (license license:gpl3+)))
;; ends here
;; [[file:emacs.org::*Package Management][]]
(define-public emacs-blamer
  (package
   (name "emacs-blamer")
   (version "0.9.4")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/Artawower/blamer.el")
           (commit (string-append "v" version))))
     (file-name (git-file-name name version))
     (sha256
      (base32 "089y8jyn78hx8pzv1q9iz94fh5b61j2jzqjnr6r91xa0s3b7kmq9"))))
   (build-system emacs-build-system)
   (arguments
    (list
     #:tests? #true
     #:test-command #~(list "eldev" "--use-emacsloadpath" "-p" "-dtT" "-C" "test")))
   (native-inputs (list emacs-eldev))
   (propagated-inputs (list emacs-posframe emacs-async))
   (home-page "https://github.com/Artawower/blamer.el")
   (synopsis "...")
   (description "...")
   (license license:gpl3)))
;; ends here
;; [[file:emacs.org::*Package Management][]]
;; currently being put into guix main...
(define-public emacs-eglot-booster
  ;; Not tagged
  (let ((commit "3f9159a8b7fe87e2f01280a2c4c98ca6dab49d13"))
    (package
      (name "emacs-eglot-booster")
      (version "0.0.2")
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/jdtsmith/eglot-booster")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32 "1aq5fpfasgm5lz8hk476c1zqyj33m024nx8w9qv4qrg88y5mq5n9"))))
      (build-system emacs-build-system)
      (home-page "https://github.com/jdtsmith/eglot-booster")
      (synopsis "Configuration to use lsp-booster with Eglot")
      (description "This package adds configuration to simplify using
@{emacs-lsp-booster} with @code{emacs-eglot}. It can be enabled with
@code{eglot-booster-mode}.")
      (license license:gpl3+))))
;; ends here
;; [[file:emacs.org::*Package Management][]]
;; custom definition with scripts in $PATH
(define emacs-apheleia
  (package
    (name "emacs-apheleia")
    (version "4.1")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url "https://github.com/raxod502/apheleia")
             (commit (string-append "v" version))))
       (file-name (git-file-name name version))
       (sha256
        (base32 "10adk4l5090dy0as6xqv5qpgdc0vf7jy8s1nrsn3zgf6n3s3ffqb"))))
    (build-system emacs-build-system)
    (arguments
     (list
      #:phases
      #~(modify-phases %standard-phases
          (add-after 'install 'add-formatters-to-path
            (lambda _
              ;; install the apheleia-npx script for use in web related things
              (install-file "scripts/formatters/apheleia-npx"
                            (string-append #$output "/bin")))))))
    (home-page "https://github.com/raxod502/apheleia")
    (synopsis "Reformat buffer stably")
    (description
     "This package allows for a buffer to be reformatted without moving point,
so that running a formatting tool like @code{Prettier} or @code{Black} upon
saving won't move point back to the beginning of the buffer.")
    (license license:gpl3+)))
;; ends here
;; [[file:emacs.org::*Package Management][]]
(define-public emacs-easy-escape
  (package
   (name "emacs-easy-escape")
   (version "0.2.1")
   (source
    (origin
     (method git-fetch)
     (uri (git-reference
           (url "https://github.com/cpitclaudel/easy-escape")
           (commit "938497a21e65ba6b3ff8ec90e93a6d0ab18dc9b4")))
     (file-name (git-file-name name version))
     (sha256
      (base32 "0bqwn6cd7lrk7f8vgcvclryvlpxvl2bndsmwmbn0zxmvqkdba7l1"))))
   (build-system emacs-build-system)
   (home-page "https://github.com/cpitclaudel/easy-escape")
   (synopsis "...")
   (description "...")
   (license license:gpl3)))
;; ends here

(define common-emacs-packages
  (->packages
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-diminish"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-general"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-dash"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-ws-butler"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-pinentry"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-sudo-edit"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-persistent-scratch"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-wgrep"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-evil-fringe-mark
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-modus-themes
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-doom-modeline"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-evil-anzu"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-rainbow-delimiters"
   "emacs-rainbow-mode"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-which-key"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-helpful"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-hl-todo"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-consult-todo
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-popper"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-vertico"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-orderless"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-marginalia"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-consult"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-corfu"
   "emacs-corfu-terminal"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-corfu-candidate-overlay"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-embark"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-yasnippet"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-nerd-icons-dired
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-diredfl"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-evil"
   "emacs-goto-chg"
   "emacs-evil-matchit"
   "emacs-evil-nerd-commenter"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-evil-collection
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-evil-goggles"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-evil-numbers"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-evil-surround"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-vterm"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-magit"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-magit-todos"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-blamer
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-diff-hl"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-lsp-booster"
   emacs-eglot-booster
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "ccls" ; language server c/c++
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-apheleia
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-editorconfig"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-flycheck"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-cmake-mode"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   emacs-easy-escape
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-eros"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-highlight-defined"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-suggest"
   "emacs-esup"
   "emacs-dockerfile-mode"
   "emacs-markdown-mode"
   "emacs-guix"
   "emacs-ellama"
   "emacs-evil-exchange"
   "emacs-snow"
   "emacs-ess"
   "emacs-plantuml-mode"
   ;; ends here
   ))

(define personal-emacs-packages
  (->packages
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-clojure-mode"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   ;; "emacs-stumpwm-mode" ; emacs mode for stump
   "emacs-sly" ; repl
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-geiser"
   "emacs-geiser-guile"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "mu"
   "isync"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-htmlize"
   ;; ends here
   ))

(define work-emacs-packages
  (->packages
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-jenkinsfile-mode"
   ;; ends here
   ;; [[file:emacs.org::*Package Management][]]
   "emacs-feature-mode"
   ;; ends here
   ))
;; Package Management:2 ends here
